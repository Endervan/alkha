-- MySQL dump 10.13  Distrib 5.5.38-35.2, for Linux (x86_64)
--
-- Host: localhost    Database: masmidia_alkha
-- ------------------------------------------------------
-- Server version	5.5.38-35.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_banners`
--

DROP TABLE IF EXISTS `tb_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banners`
--

LOCK TABLES `tb_banners` WRITE;
/*!40000 ALTER TABLE `tb_banners` DISABLE KEYS */;
INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`) VALUES (34,'Index - Banner 1','1410201512169835812275.jpg','SIM',NULL,'1','index--banner-1',''),(35,'Index Banner 2','0511201510525267097867.jpg','SIM',NULL,'1','index-banner-2',''),(36,'Mobile Banner 1','1410201501057978330871.jpg','SIM',NULL,'2','mobile-banner-1',''),(37,'Mobile Banner 2','0511201511039904805595.jpg','SIM',NULL,'2','mobile-banner-2',''),(38,'index Banner -3','0511201510507711607304.jpg','SIM',NULL,'1','index-banner-3',''),(39,'Mobile Banner 3','0511201511058611254352.jpg','SIM',NULL,'2','mobile-banner-3','');
/*!40000 ALTER TABLE `tb_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_categorias_portifolios`
--

DROP TABLE IF EXISTS `tb_categorias_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categorias_portifolios` (
  `idcategoriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaportifolio`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categorias_portifolios`
--

LOCK TABLES `tb_categorias_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_categorias_portifolios` DISABLE KEYS */;
INSERT INTO `tb_categorias_portifolios` (`idcategoriaportifolio`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `classe`) VALUES (49,'Esquadrias','imagem_nao_disponivel.jpg','SIM',NULL,'esquadrias',NULL,NULL,NULL,NULL),(50,'Portões','imagem_nao_disponivel.jpg','NAO',NULL,'portoes',NULL,NULL,NULL,NULL),(51,'Janelas','imagem_nao_disponivel.jpg','NAO',NULL,'janelas',NULL,NULL,NULL,NULL),(52,'Pele de Vidro','imagem_nao_disponivel.jpg','SIM',NULL,'pele-de-vidro',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_categorias_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_clientes`
--

DROP TABLE IF EXISTS `tb_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` longtext COLLATE utf8_unicode_ci,
  `keywords_google` longtext COLLATE utf8_unicode_ci,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_clientes`
--

LOCK TABLES `tb_clientes` WRITE;
/*!40000 ALTER TABLE `tb_clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_comentarios_dicas`
--

DROP TABLE IF EXISTS `tb_comentarios_dicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentariodica`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_comentarios_dicas`
--

LOCK TABLES `tb_comentarios_dicas` WRITE;
/*!40000 ALTER TABLE `tb_comentarios_dicas` DISABLE KEYS */;
INSERT INTO `tb_comentarios_dicas` (`idcomentariodica`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_dica`, `data`) VALUES (16,'Marcio André','marcio@masmidia.com.br','Marcio André deixou o comentário Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-10'),(17,'Antônio','antonio@masmidia.com.br','Antonio deixou o comentário Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-11'),(18,'Ana Clara','ana@masmidia.com.br','Ana Clara Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-13'),(19,'Anita','anita@masmidia.com.br','Aqui fica a mensagem do usuário.','SIM',0,' ',42,'2015-10-14');
/*!40000 ALTER TABLE `tb_comentarios_dicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_configuracoes`
--

DROP TABLE IF EXISTS `tb_configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `msg_deposito_bancario` longtext,
  `place` longtext,
  `url_maps` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `src_place` longtext,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_configuracoes`
--

LOCK TABLES `tb_configuracoes` WRITE;
/*!40000 ALTER TABLE `tb_configuracoes` DISABLE KEYS */;
INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `latitude`, `longitude`, `msg_deposito_bancario`, `place`, `url_maps`, `email_copia`, `src_place`) VALUES (1,'Alkha Esquadrias de Alumínio Brasília - DF, Portas de Alumínio, Janelas de Alumínio, Portões de Alumínio, Brises, Pele de vidro, Revestimento ACM','Show-room e fábrica de esquadrias de alumínio em Brasília - DF. Com Loja própria especializada em esquadrias, portas, janelas, portões de alumínio, brises, fachadas, pele de vidro, Structural Glazing e toda a linha de produtos e serviços em esquadrias de alumínio para os mais diversos projetos. Atendimento a pequenas, médias e grandes obras.','Esquadrias de Alumínio, Esquadrias de Alumínio Brasília DF,Esquadria de Alumínio DF,Portas de Alumínio,Portas de Alumínio Brasília DF,Janelas de Alumínio,Janelas de Alumínio Brasília DF,Portões de Alumínio,Portões de Alumínio Brasília DF,Esquadrias de Alumínio Sob Medida Brasília DF','SIM',0,'alkha-esquadrias-de-aluminio-brasilia--df-portas-de-aluminio-janelas-de-aluminio-portoes-de-aluminio-brises-pele-de-vidro-revestimento-acm','ADE conjunto 17 lote 01 - Águas Claras - DF','(61) 3404-5828','','marciomas@gmail.com','','',NULL,NULL,NULL,'junior@homewebbrasil.com.br','https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15350.483017031958!2d-48.010962!3d-15.8765176!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb701df06ea14a43a!2sAlkha+Alum%C3%ADnio+-+Esquadrias+de+Alum%C3%ADnio+Bras%C3%ADlia!5e0!3m2!1spt-BR!2sbr!4v1451918821700');
/*!40000 ALTER TABLE `tb_configuracoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_depoimentos`
--

DROP TABLE IF EXISTS `tb_depoimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `depoimento` varchar(255) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `dt_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_depoimentos`
--

LOCK TABLES `tb_depoimentos` WRITE;
/*!40000 ALTER TABLE `tb_depoimentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_dicas`
--

DROP TABLE IF EXISTS `tb_dicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_dicas`
--

LOCK TABLES `tb_dicas` WRITE;
/*!40000 ALTER TABLE `tb_dicas` DISABLE KEYS */;
INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES (42,'Dicas para comprar esquadrias de Aluminio','<p>\r\n	Dica 1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509556659832795..jpg','SIM',NULL,'dicas-para-comprar-esquadrias-de-aluminio',NULL,NULL,NULL,NULL),(43,'Dica 2','<p>\r\n	Dica 2&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509555472878387..jpg','SIM',NULL,'dica-2',NULL,NULL,NULL,NULL),(44,'Dica 3','<p>\r\n	Dica 3&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509556170363434..jpg','SIM',NULL,'dica-3',NULL,NULL,NULL,NULL),(45,'Dica 4','<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509553953337102..jpg','SIM',NULL,'dica-4',NULL,NULL,NULL,NULL),(46,'Dica 5','<p>\r\n	5&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509565442976086..jpg','SIM',NULL,'dica-5',NULL,NULL,NULL,NULL),(47,'Dica 6','<p>\r\n	6&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>','1310201509562891442821..gif','SIM',NULL,'dica-6',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_dicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_empresa`
--

DROP TABLE IF EXISTS `tb_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  `src_place` longtext,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_empresa`
--

LOCK TABLES `tb_empresa` WRITE;
/*!40000 ALTER TABLE `tb_empresa` DISABLE KEYS */;
INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`, `src_place`) VALUES (1,'CONHEÇA MAIS A ALKHA ','<p>\r\n	A Alkha Esquadrias com mais de dez anos de atua&ccedil;&atilde;o no mercado de Bras&iacute;lia, a empresa trabalha com grandes parcerias, fornecedores de mat&eacute;ria prima e com maquin&aacute;rios nacionais e importados.Possui sede pr&oacute;pria com show-room e f&aacute;brica de esquadrias de alum&iacute;nio.</p>\r\n<p>\r\n	Atende o Distrito Federal e outros estados. Aqui voc&ecirc; pode trazer o seu projeto de esquadria ou contar com nosso projetista, encarregado de formular os melhores projetos dentro da sua necessidade. Nosso objetivo &eacute; proporcionar a melhoria continua do padr&atilde;o de qualidade e produtividade de esquadrias de alum&iacute;nio, atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os especializados.</p>\r\n<p>\r\n	A Alkha Esquadrias &eacute; uma empresa extremante comprometida com o bom atendimento, buscando sempre novidades para adequar-se &agrave;s necessidades de nossos clientes. Temos como intuito n&atilde;o apenas atender, mas sim superar as expectativas de nossos clientes, oferecendo credibilidade, seguran&ccedil;a, conforto e tranq&uuml;ilidade. Para estabelecermos rela&ccedil;&otilde;es de confian&ccedil;a com nossos clientes, de forma que estes possam sempre contar com uma fonte confi&aacute;vel, r&aacute;pida e eficiente para suas necessidades t&eacute;cnicas.</p>','SIM',0,'','','','conheca-mais-a-alkha-',NULL,NULL,NULL,NULL),(2,'FORNECEDORES - TEXTO','<p>\r\n	VEJA ABAIXO OS FORNECEDORES QUE SE TORNARAM PARCEIROS DA ALKHA</p>','SIM',0,'','','','fornecedores--texto',NULL,NULL,NULL,NULL),(3,'CONTATO - Frase lateral','<p>\r\n	TRAGA-NOS O SEU PROJETO OU CONTE COM NOSSO PROJETISTA, ENCARREGADO DE FORMULAR OS MELHORES PROJETOS DENTRO DA SUA NECESSIDADE.</p>','SIM',0,'','','','contato--frase-lateral',NULL,NULL,NULL,NULL),(4,'Orçamento - texto','<p>\r\n	TRAGA-NOS O SEU PROJETO OU CONTE COM NOSSO PROJETISTA, ENCARREGADO DE FORMULAR OS MELHORES PROJETOS DENTRO DA SUA NECESSIDADE.</p>','SIM',0,'','','','orcamento--texto',NULL,NULL,NULL,NULL),(5,'Index - Portifólio','<p>\r\n	CONFIRA ALGUNS DE NOSSOS SERVI&Ccedil;OS REALIZADOS EM DIVERSOS SETORES DA CONSTRU&Ccedil;&Atilde;O</p>','SIM',0,'','','','index--portifolio',NULL,NULL,NULL,NULL),(6,'Index - CONHEÇA MAIS A ALKHA','<div>\r\n	A Alkha com mais de dez anos de atua&ccedil;&atilde;o no mercado de Bras&iacute;lia, a empresa trabalha com grandes parcerias, fornecedores de mat&eacute;ria prima e com maquin&aacute;rios nacionais e importados.</div>\r\n<div>\r\n	Possui sede pr&oacute;pria com show-room e f&aacute;brica de esquadrias de alum&iacute;nio.</div>\r\n<div>\r\n	Atende o Distrito Federal e outros estados. Aqui voc&ecirc; pode trazer o seu projeto de esquadria ou contar com nosso projetista, encarregado de formular os melhores projetos dentro da sua necessidade. Nosso objetivo &eacute; proporcionar a melhoria continua do padr&atilde;o de qualidade e produtividade de esquadrias de alum&iacute;nio, atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os especializados.</div>','SIM',0,'','','','index--conheca-mais-a-alkha',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fornecedores`
--

DROP TABLE IF EXISTS `tb_fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fornecedores` (
  `idfornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idfornecedor`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fornecedores`
--

LOCK TABLES `tb_fornecedores` WRITE;
/*!40000 ALTER TABLE `tb_fornecedores` DISABLE KEYS */;
INSERT INTO `tb_fornecedores` (`idfornecedor`, `titulo`, `imagem`, `title_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `telefone`, `url`) VALUES (148,'Alcoa','1912201512459459617307..jpg','','','SIM',0,'alcoa','0800 015 9888',NULL),(149,'Albra','1912201512517052933155..jpg','','','SIM',0,'albra','( 61 ) 3462-8500',NULL),(150,'Fornecedor 3','1310201506212588493703..jpg','','','SIM',0,'fornecedor-3','(61)4538-0987',NULL),(151,'Fornecedor 4','1310201506217370250473..jpg','','','SIM',0,'fornecedor-4','(61)4533-0909',NULL),(152,'Fornecedor 5','1310201506234057518086..jpg','','','SIM',0,'fornecedor-5','(61)3342-0987',NULL);
/*!40000 ALTER TABLE `tb_fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_galerias_portifolios`
--

DROP TABLE IF EXISTS `tb_galerias_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaportifolio`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_galerias_portifolios`
--

LOCK TABLES `tb_galerias_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_galerias_portifolios` DISABLE KEYS */;
INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES (49,'2701201612552515951767.jpg','SIM',NULL,NULL,1),(50,'2701201612555322323114.jpg','SIM',NULL,NULL,1),(51,'2701201612559385792641.jpg','SIM',NULL,NULL,1),(52,'2701201612551951997780.jpg','SIM',NULL,NULL,1),(53,'2701201612553863505253.jpg','SIM',NULL,NULL,1),(54,'2701201612559513220075.jpg','SIM',NULL,NULL,1),(55,'2801201611333019064590.jpg','SIM',NULL,NULL,2),(56,'2801201611339135590572.jpg','SIM',NULL,NULL,2),(57,'2801201611335134256825.jpg','SIM',NULL,NULL,2),(58,'2801201611334976963814.jpg','SIM',NULL,NULL,2),(59,'2801201611339556588066.jpg','SIM',NULL,NULL,2),(60,'2801201611424112164444.jpg','SIM',NULL,NULL,3),(61,'2801201611435018171349.jpg','SIM',NULL,NULL,3),(62,'2801201611435659002703.jpg','SIM',NULL,NULL,3),(63,'2801201611435890847129.jpg','SIM',NULL,NULL,3),(64,'2801201611432321434625.jpg','SIM',NULL,NULL,3),(65,'2801201611475084387701.jpg','SIM',NULL,NULL,4),(66,'2801201611482683978291.jpg','SIM',NULL,NULL,4),(67,'2801201611486948757660.jpg','SIM',NULL,NULL,4),(68,'2801201611486187683679.jpg','SIM',NULL,NULL,4),(69,'2801201612225498684648.jpg','SIM',NULL,NULL,5),(70,'2801201612226545408819.jpg','SIM',NULL,NULL,5),(71,'2801201612228658725255.jpg','SIM',NULL,NULL,5),(72,'2801201612222515558650.jpg','SIM',NULL,NULL,5),(73,'2801201612225416350021.jpg','SIM',NULL,NULL,5),(74,'2801201612277345627285.jpg','SIM',NULL,NULL,7),(75,'2801201612276295744917.jpg','SIM',NULL,NULL,7),(76,'2801201612274921043113.jpg','SIM',NULL,NULL,7),(77,'2801201612273695446087.jpg','SIM',NULL,NULL,7),(78,'2801201612277162703292.jpg','SIM',NULL,NULL,7),(79,'2801201612276289139348.jpg','SIM',NULL,NULL,7),(80,'2801201612278862790808.jpg','SIM',NULL,NULL,7);
/*!40000 ALTER TABLE `tb_galerias_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logins`
--

DROP TABLE IF EXISTS `tb_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logins`
--

LOCK TABLES `tb_logins` WRITE;
/*!40000 ALTER TABLE `tb_logins` DISABLE KEYS */;
INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES (1,'HomeWeb','e10adc3949ba59abbe56e057f20f883e','SIM',0,'atendimento.sites@homewebbrasil.com.br');
/*!40000 ALTER TABLE `tb_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logs_logins`
--

DROP TABLE IF EXISTS `tb_logs_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB AUTO_INCREMENT=1709 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logs_logins`
--

LOCK TABLES `tb_logs_logins` WRITE;
/*!40000 ALTER TABLE `tb_logs_logins` DISABLE KEYS */;
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES (1580,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:01:00',5),(1581,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:02:10',5),(1582,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:02:57',5),(1583,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:25',1),(1584,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:33',1),(1585,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:41',1),(1586,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:48',1),(1587,'CADASTRO DO CLIENTE ','','2015-09-29','16:03:55',1),(1588,'CADASTRO DO CLIENTE ','','2015-09-29','16:04:19',1),(1589,'CADASTRO DO CLIENTE ','','2015-09-29','16:04:43',1),(1590,'CADASTRO DO CLIENTE ','','2015-09-29','16:11:11',1),(1591,'CADASTRO DO CLIENTE ','','2015-09-30','20:23:24',1),(1592,'CADASTRO DO CLIENTE ','','2015-09-30','20:23:56',1),(1593,'CADASTRO DO CLIENTE ','','2015-09-30','20:26:24',1),(1594,'CADASTRO DO CLIENTE ','','2015-09-30','20:27:36',1),(1595,'CADASTRO DO CLIENTE ','','2015-09-30','20:29:37',1),(1596,'ALTERAÇÃO DO CLIENTE ','','2015-09-30','20:39:32',1),(1597,'CADASTRO DO CLIENTE ','','2015-09-30','20:50:23',1),(1598,'CADASTRO DO CLIENTE ','','2015-10-13','16:29:17',5),(1599,'CADASTRO DO CLIENTE ','','2015-10-13','16:53:37',5),(1600,'CADASTRO DO CLIENTE ','','2015-10-13','16:53:54',5),(1601,'CADASTRO DO CLIENTE ','','2015-10-13','16:54:00',5),(1602,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:54:12',5),(1603,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:54:17',5),(1604,'CADASTRO DO CLIENTE ','','2015-10-13','16:54:53',5),(1605,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:55:07',5),(1606,'CADASTRO DO CLIENTE ','','2015-10-13','17:02:26',5),(1607,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:03:37',5),(1608,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:17:15',5),(1609,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:17:23',5),(1610,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:19:05',5),(1611,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:26:44',5),(1612,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:39:10',5),(1613,'CADASTRO DO CLIENTE ','','2015-10-13','18:13:51',1),(1614,'CADASTRO DO CLIENTE ','','2015-10-13','18:20:48',1),(1615,'CADASTRO DO CLIENTE ','','2015-10-13','18:21:03',1),(1616,'CADASTRO DO CLIENTE ','','2015-10-13','18:21:22',1),(1617,'CADASTRO DO CLIENTE ','','2015-10-13','18:23:08',1),(1618,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:40:31',1),(1619,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:43:52',1),(1620,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:44:59',1),(1621,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:46:05',1),(1622,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:10',1),(1623,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:25',1),(1624,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:42',1),(1625,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:54',1),(1626,'CADASTRO DO CLIENTE ','','2015-10-13','21:56:07',1),(1627,'CADASTRO DO CLIENTE ','','2015-10-13','21:56:25',1),(1628,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','22:53:14',1),(1629,'CADASTRO DO CLIENTE ','','2015-10-13','23:53:22',1),(1630,'CADASTRO DO CLIENTE ','','2015-10-13','23:54:31',1),(1631,'CADASTRO DO CLIENTE ','','2015-10-13','23:55:28',1),(1632,'ALTERAÇÃO DO CLIENTE ','','2015-10-14','00:42:55',1),(1633,'ALTERAÇÃO DO CLIENTE ','','2015-10-14','01:11:23',1),(1634,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:26:00',1),(1635,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:26:50',1),(1636,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:27:25',1),(1637,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:28:36',1),(1638,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:29:04',1),(1639,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:29:50',1),(1640,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:30:32',1),(1641,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:31:51',1),(1642,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:32:32',1),(1643,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:34:03',1),(1644,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:34:44',1),(1645,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:36:26',1),(1646,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:43:54',1),(1647,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:45:18',1),(1648,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:51:15',1),(1649,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:47:35',1),(1650,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:49:39',1),(1651,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:52:23',1),(1652,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:53:43',1),(1653,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:54:26',1),(1654,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:57:51',1),(1655,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:58:30',1),(1656,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:03:07',1),(1657,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:03:51',1),(1658,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:04:59',1),(1659,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:10:18',1),(1660,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:10:56',1),(1661,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:11:36',1),(1662,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:14:51',1),(1663,'CADASTRO DO CLIENTE ','','2016-01-27','12:45:08',1),(1664,'ALTERAÇÃO DO CLIENTE ','','2016-01-27','12:50:17',1),(1665,'EXCLUSÃO DO LOGIN 3, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'3\'','2016-01-27','13:08:15',1),(1666,'EXCLUSÃO DO LOGIN 4, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'','2016-01-27','13:08:29',1),(1667,'EXCLUSÃO DO LOGIN 5, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'','2016-01-27','13:08:41',1),(1668,'EXCLUSÃO DO LOGIN 6, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'6\'','2016-01-27','13:08:52',1),(1669,'ALTERAÇÃO DO CLIENTE ','','2016-01-27','13:16:18',1),(1670,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:30:41',1),(1671,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:31:11',1),(1672,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:32:14',1),(1673,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:00',1),(1674,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:21',1),(1675,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:52',1),(1676,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:36:05',1),(1677,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:42:21',1),(1678,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:47:34',1),(1679,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:52:01',1),(1680,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:53:00',1),(1681,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:54:06',1),(1682,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:21:47',1),(1683,'CADASTRO DO CLIENTE ','','2016-01-28','12:26:52',1),(1684,'EXCLUSÃO DO LOGIN 6, NOME: , Email: ','DELETE FROM tb_portifolios WHERE idportifolio = \'6\'','2016-01-28','12:27:46',1),(1685,'DESATIVOU O LOGIN 50','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'50\'','2016-01-28','12:31:40',1),(1686,'DESATIVOU O LOGIN 51','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'51\'','2016-01-28','12:31:54',1),(1687,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:33:07',1),(1688,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:33:59',1),(1689,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:34:10',1),(1690,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:37:50',1),(1691,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:38:43',1),(1692,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:40:02',1),(1693,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:42:16',1),(1694,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:42:51',1),(1695,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:43:18',1),(1696,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:43:51',1),(1697,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:44:18',1),(1698,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:44:40',1),(1699,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:45:11',1),(1700,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:45:53',1),(1701,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:46:30',1),(1702,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:53:54',1),(1703,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:54:24',1),(1704,'EXCLUSÃO DO LOGIN 9, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'9\'','2016-01-28','13:06:05',1),(1705,'EXCLUSÃO DO LOGIN 10, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'10\'','2016-01-28','13:06:11',1),(1706,'EXCLUSÃO DO LOGIN 11, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'11\'','2016-01-28','13:06:17',1),(1707,'EXCLUSÃO DO LOGIN 12, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'12\'','2016-01-28','13:06:23',1),(1708,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','13:09:33',1);
/*!40000 ALTER TABLE `tb_logs_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_portifolios`
--

DROP TABLE IF EXISTS `tb_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portifolios` (
  `idportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_categoriaportifolio` int(11) DEFAULT NULL,
  `cliente` varchar(255) DEFAULT NULL,
  `tipo_servico` varchar(255) DEFAULT NULL,
  `data_feito` date DEFAULT NULL,
  `feito_por` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idportifolio`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_portifolios`
--

LOCK TABLES `tb_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_portifolios` DISABLE KEYS */;
INSERT INTO `tb_portifolios` (`idportifolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaportifolio`, `cliente`, `tipo_servico`, `data_feito`, `feito_por`) VALUES (1,'E-BUSINESS','2701201612508467057690..jpg','',NULL,NULL,NULL,'SIM',NULL,'ebusiness',52,'E-BUSINESS - Pele de Vidro Structural Glazing','Pele de Vidro','0000-00-00','Alkha Esquadrias'),(2,'ED. CÔTE DAZUR','2801201611304812968294..jpg','',NULL,NULL,NULL,'SIM',NULL,'ed-cote-dazur',49,'ED. CÔTE D´AZUR','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(3,'DUE CAPRI e MURANO','2801201611428611047743..jpg','',NULL,NULL,NULL,'SIM',NULL,'due-capri-e-murano',49,'Ed. DUE CAPRI e MURANO','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(4,'ED. LE-CLUB','2801201611476114530012..jpg','',NULL,NULL,NULL,'SIM',NULL,'ed-leclub',49,'LE-CLUB','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(5,'VITRIUM ODEBRECHT','2801201612218140104611..jpg','',NULL,NULL,NULL,'SIM',NULL,'vitrium-odebrecht',49,'VITRIUM - ODEBRECHT','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(7,'OBRAS RESIDENCIAIS','2801201612268257425087..jpg','',NULL,NULL,NULL,'SIM',NULL,'obras-residenciais',49,'OBRAS RESIDENCIAIS','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias');
/*!40000 ALTER TABLE `tb_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_produtos`
--

DROP TABLE IF EXISTS `tb_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_produtos`
--

LOCK TABLES `tb_produtos` WRITE;
/*!40000 ALTER TABLE `tb_produtos` DISABLE KEYS */;
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES (146,'Brises','0401201601106939749336..jpg','<p>\r\n	Brises s&atilde;o particularmente &uacute;teis para fachadas, quando se necessita diminuir a a&ccedil;&atilde;o da luz direta ou quando se requer privacidade sem perder a luminosidade, ventila&ccedil;&atilde;o e vis&atilde;o exterior.</p>\r\n<p>\r\n	Funciona ainda como elemento decorativo para fachadas, dando um ar de leveza mesmo quando utilizado em grandes pain&eacute;is.</p>','','','','SIM',0,'brises',0,NULL,NULL,NULL),(147,'Revestimento ACM','3009201508231400491918..jpg','<p>\r\n	Material inovador e nobre, o ACM &eacute; um produto com certifica&ccedil;&atilde;o ISO-9002 &nbsp;revolucion&aacute;rio e vers&aacute;til produzido com a mais alta tecnologia. &Eacute; rigorosamente plano e ao mesmo tempo de alta conformabilidade, ou seja, dobr&aacute;-lo mediante usinagem ou curv&aacute;-lo atrav&eacute;s calandra. &nbsp; Usado como revestimento de &nbsp;edifica&ccedil;&otilde;es externos e internos, postos de gasolinas, quiosques, Bancos etc.</p>\r\n<p>\r\n	Os pain&eacute;is ACM s&atilde;o compostos de duas laminas de alum&iacute;nio unidas a um nucleo termoplastico de polietileno de baixa densidade. S&atilde;o fabricados em um processo continuo em placas de 3, 4, 5, 6, 8 mm de espessura, com v&aacute;rias combina&ccedil;&otilde;es de comprimentos e larguras. o paniel acm &eacute; tambem conhecido como alucobond.</p>','','','','SIM',0,'revestimento-acm',0,NULL,NULL,NULL),(148,'Portões de Alumínio','0401201601148757055935..jpg','<p>\r\n	Trabalhamos com os mais diversos projetos de port&otilde;es: alum&iacute;nio anodizado e pintura. Port&otilde;es basculantes, deslizantes e pivotantes.</p>','','','','SIM',0,'portoes-de-aluminio',0,NULL,NULL,NULL),(149,'Pele de Vidro','0401201601104124450720.jpeg','<p>\r\n	A t&eacute;cnica de &quot;Pele de Vidro&quot; &eacute; muito usada pelos arquitetos e engenheiros para proporcionar grandes aberturas, criando fachadas atraentes e consistentes. Esta t&eacute;cnica se aplica com o objetivo de permitir uma instala&ccedil;&atilde;o mais &aacute;gil, econ&ocirc;mica e de melhor veda&ccedil;&atilde;o quando comparada &agrave; metodologia tradicional. &nbsp;A instala&ccedil;&atilde;o padr&atilde;o se faz com quadros pr&eacute;-montados que chegam &agrave; obra com os vidros e borrachas previamente fixados e prontos para o encaixe nas travessas que se fixam na face frontal das colunas.<span id=\\\"\\\\&quot;cke_bm_113E\\\\&quot;\\\" span=\\\"\\\">&nbsp;</span></p>','','','','SIM',0,'pele-de-vidro',0,NULL,NULL,NULL),(150,'Portas de Alumínio','0401201601032423454121..jpg','<div>\r\n	Trabalhamos com os mais variados tipos de portas para atender suas necessidades ex: portas de abrir e correr. Tipo, pivotante, integrada, anti - ru&iacute;do, e etc.</div>\r\n<div>\r\n	Solicite uma visita de nosso profissional</div>','','','','SIM',0,'portas-de-aluminio',0,NULL,NULL,NULL),(151,'Janelas de Alumínio','0401201601044217003314..jpg','<div>\r\n	Trabalhamos com os mais variados tipos de janelas para atender suas necessidades ex: janelas de abrir e correr. Tipo basculante, maxim-ar, pivotante, fixa, integrada, anti - ru&iacute;do, grades de prote&ccedil;&atilde;o para janelas e etc.</div>\r\n<div>\r\n	Solicite uma visita de nosso profissional</div>','','','','SIM',0,'janelas-de-aluminio',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'masmidia_alkha'
--

--
-- Dumping routines for database 'masmidia_alkha'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-29 12:02:15

-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: alkhaaluminio
-- ------------------------------------------------------
-- Server version	5.5.57-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_banners`
--

DROP TABLE IF EXISTS `tb_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banners`
--

LOCK TABLES `tb_banners` WRITE;
/*!40000 ALTER TABLE `tb_banners` DISABLE KEYS */;
INSERT INTO `tb_banners` VALUES (34,'Index - Banner 1','2901201605469480447322.jpg','SIM',NULL,'1','index--banner-1',''),(35,'Index Banner 2','2901201605599691953762.jpg','SIM',NULL,'1','index-banner-2',''),(36,'Mobile Banner 1','2901201606302770562627.jpg','SIM',NULL,'2','mobile-banner-1',''),(37,'Mobile Banner 2','2901201606289687961700.jpg','SIM',NULL,'2','mobile-banner-2',''),(38,'index Banner -3','2901201605565213634694.jpg','SIM',NULL,'1','index-banner-3',''),(39,'Mobile Banner 3','2901201606251535906104.jpg','SIM',NULL,'2','mobile-banner-3','');
/*!40000 ALTER TABLE `tb_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_categorias_portifolios`
--

DROP TABLE IF EXISTS `tb_categorias_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_categorias_portifolios` (
  `idcategoriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaportifolio`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_categorias_portifolios`
--

LOCK TABLES `tb_categorias_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_categorias_portifolios` DISABLE KEYS */;
INSERT INTO `tb_categorias_portifolios` VALUES (53,'Obras Realizadas','imagem_nao_disponivel.jpg','SIM',NULL,'obras-realizadas',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_categorias_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_clientes`
--

DROP TABLE IF EXISTS `tb_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_clientes` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` longtext COLLATE utf8_unicode_ci,
  `keywords_google` longtext COLLATE utf8_unicode_ci,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_clientes`
--

LOCK TABLES `tb_clientes` WRITE;
/*!40000 ALTER TABLE `tb_clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_comentarios_dicas`
--

DROP TABLE IF EXISTS `tb_comentarios_dicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentariodica`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_comentarios_dicas`
--

LOCK TABLES `tb_comentarios_dicas` WRITE;
/*!40000 ALTER TABLE `tb_comentarios_dicas` DISABLE KEYS */;
INSERT INTO `tb_comentarios_dicas` VALUES (16,'Marcio André','marcio@masmidia.com.br','Marcio André deixou o comentário Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-10'),(17,'Antônio','antonio@masmidia.com.br','Antonio deixou o comentário Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-11'),(18,'Ana Clara','ana@masmidia.com.br','Ana Clara Lorem Ipsum is simply dummy text of the printing and typeset ting industry. Lorem Ipsum has been the industry\'s standard d ummy text ever since the 1500s, when an unknow','SIM',0,' ',42,'2015-10-13'),(19,'Anita','anita@masmidia.com.br','Aqui fica a mensagem do usuário.','SIM',0,' ',42,'2015-10-14');
/*!40000 ALTER TABLE `tb_comentarios_dicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_configuracoes`
--

DROP TABLE IF EXISTS `tb_configuracoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `msg_deposito_bancario` longtext,
  `place` longtext,
  `url_maps` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `src_place` longtext,
  `google_plus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_configuracoes`
--

LOCK TABLES `tb_configuracoes` WRITE;
/*!40000 ALTER TABLE `tb_configuracoes` DISABLE KEYS */;
INSERT INTO `tb_configuracoes` VALUES (1,'Alkha Esquadrias de Alumínio Brasília - DF, Portas de Alumínio, Janelas de Alumínio, Portões de Alumínio, Brises, Pele de vidro, Revestimento ACM','Show-room e fábrica de esquadrias de alumínio em Brasília - DF. Com Loja própria especializada em esquadrias, portas, janelas, portões de alumínio, brises, fachadas, pele de vidro, Structural Glazing e toda a linha de produtos e serviços em esquadrias de alumínio para os mais diversos projetos. Atendimento a pequenas, médias e grandes obras.','Esquadrias de Alumínio, Esquadrias de Alumínio Brasília DF,Esquadria de Alumínio DF,Portas de Alumínio,Portas de Alumínio Brasília DF,Janelas de Alumínio,Janelas de Alumínio Brasília DF,Portões de Alumínio,Portões de Alumínio Brasília DF,Esquadrias de Alumínio Sob Medida Brasília DF','SIM',0,'alkha-esquadrias-de-aluminio-brasilia--df-portas-de-aluminio-janelas-de-aluminio-portoes-de-aluminio-brises-pele-de-vidro-revestimento-acm','ADE conjunto 17 lote 01 - Águas Claras - DF','(61) 3404-5828','','alkha@grupoalkha.com.br, junior@homewebbrasil.com.br','','',NULL,NULL,NULL,'angela.homeweb@gmail.com, marciomas@gmail.com','https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15350.483017031958!2d-48.010962!3d-15.8765176!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb701df06ea14a43a!2sAlkha+Alum%C3%ADnio+-+Esquadrias+de+Alum%C3%ADnio+Bras%C3%ADlia!5e0!3m2!1spt-BR!2sbr!4v1451918821700','https://plus.google.com/100547666664659393846');
/*!40000 ALTER TABLE `tb_configuracoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_depoimentos`
--

DROP TABLE IF EXISTS `tb_depoimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `depoimento` varchar(255) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `dt_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_depoimentos`
--

LOCK TABLES `tb_depoimentos` WRITE;
/*!40000 ALTER TABLE `tb_depoimentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_dicas`
--

DROP TABLE IF EXISTS `tb_dicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_dicas`
--

LOCK TABLES `tb_dicas` WRITE;
/*!40000 ALTER TABLE `tb_dicas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_dicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_empresa`
--

DROP TABLE IF EXISTS `tb_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  `src_place` longtext,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_empresa`
--

LOCK TABLES `tb_empresa` WRITE;
/*!40000 ALTER TABLE `tb_empresa` DISABLE KEYS */;
INSERT INTO `tb_empresa` VALUES (1,'CONHEÇA MAIS A ALKHA ','<p>\r\n	A Alkha Esquadrias, com mais de dez anos de atua&ccedil;&atilde;o no mercado de Bras&iacute;lia, trabalha com grandes parcerias, fornecedores de mat&eacute;ria prima e com maquin&aacute;rios nacionais e importados. Possui sede pr&oacute;pria com show-room e f&aacute;brica de esquadrias de alum&iacute;nio, atendendo o Distrito Federal e outros estados.&nbsp;</p>\r\n<p>\r\n	Aqui voc&ecirc; pode trazer o seu projeto de esquadria ou contar com nosso projetista, encarregado de formular os melhores projetos dentro da sua necessidade. Nosso objetivo &eacute; proporcionar a melhoria cont&iacute;nua do padr&atilde;o de qualidade e produtividade de esquadrias de alum&iacute;nio, atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os especializados.</p>\r\n<p>\r\n	A Alkha Esquadrias &eacute; uma empresa extremante comprometida com o bom atendimento, buscando sempre novidades para adequar-se &agrave;s necessidades de nossos clientes. Temos como intuito n&atilde;o apenas atender, mas sim superar as expectativas de nossos clientes, oferecendo credibilidade, seguran&ccedil;a, conforto e tranq&uuml;ilidade para estabelecermos rela&ccedil;&otilde;es de confian&ccedil;a com nossos clientes, de forma que estes possam sempre contar com uma fonte confi&aacute;vel, r&aacute;pida e eficiente para suas necessidades t&eacute;cnicas.</p>','SIM',0,'','','','conheca-mais-a-alkha-',NULL,NULL,NULL,NULL),(2,'FORNECEDORES - TEXTO','<p>\r\n	VEJA ABAIXO OS FORNECEDORES QUE SE TORNARAM PARCEIROS DA ALKHA</p>','SIM',0,'','','','fornecedores--texto',NULL,NULL,NULL,NULL),(3,'CONTATO - Frase lateral','<p>\r\n	TRAGA-NOS O SEU PROJETO OU CONTE COM NOSSO PROJETISTA, ENCARREGADO DE FORMULAR OS MELHORES PROJETOS DENTRO DA SUA NECESSIDADE.</p>','SIM',0,'','','','contato--frase-lateral',NULL,NULL,NULL,NULL),(4,'Orçamento - texto','<p>\r\n	TRAGA-NOS O SEU PROJETO OU CONTE COM NOSSO PROJETISTA, ENCARREGADO DE FORMULAR OS MELHORES PROJETOS DENTRO DA SUA NECESSIDADE.</p>','SIM',0,'','','','orcamento--texto',NULL,NULL,NULL,NULL),(5,'Index - Portifólio','<p>\r\n	CONFIRA ALGUNS DE NOSSOS SERVI&Ccedil;OS REALIZADOS EM DIVERSOS SETORES DA CONSTRU&Ccedil;&Atilde;O</p>','SIM',0,'','','','index--portifolio',NULL,NULL,NULL,NULL),(6,'Index - CONHEÇA MAIS A ALKHA','<div>\r\n	A Alkha, com mais de dez anos de atua&ccedil;&atilde;o no mercado de Bras&iacute;lia, trabalha com grandes parcerias, fornecedores de mat&eacute;ria prima e com maquin&aacute;rios nacionais e importados.</div>\r\n<div>\r\n	Possui sede pr&oacute;pria com show-room e f&aacute;brica de esquadrias de alum&iacute;nio, atendendo o Distrito Federal e outros estados.</div>\r\n<div>\r\n	Aqui voc&ecirc; pode trazer o seu projeto de esquadria ou contar com nosso projetista, encarregado de formular os melhores projetos dentro da sua necessidade. Nosso objetivo &eacute; proporcionar a melhoria cont&iacute;nua do padr&atilde;o de qualidade e produtividade de esquadrias de alum&iacute;nio, atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os especializados.</div>','SIM',0,'','','','index--conheca-mais-a-alkha',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fornecedores`
--

DROP TABLE IF EXISTS `tb_fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fornecedores` (
  `idfornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idfornecedor`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fornecedores`
--

LOCK TABLES `tb_fornecedores` WRITE;
/*!40000 ALTER TABLE `tb_fornecedores` DISABLE KEYS */;
INSERT INTO `tb_fornecedores` VALUES (148,'Alcoa','1912201512459459617307..jpg','','','SIM',0,'alcoa','0800 015 9888',NULL),(149,'Albra','1912201512517052933155..jpg','','','SIM',0,'albra','( 61 ) 3462-8500',NULL),(150,'Albra Color Alumínio','3101201601322767869076..jpg','','','SIM',0,'albra-color-aluminio','( 61 ) 3623-7070',NULL),(151,'GIESSE','3101201601451905722440..jpg','','','SIM',0,'giesse','( 11 ) 4512-6800',NULL),(152,'FISE','3101201601439380747125..jpg','','','SIM',0,'fise','( 11 ) 5545-2680',NULL),(153,'UDINESE','3101201601492335414621..jpg','','','SIM',0,'udinese','0800 701 2200',NULL),(154,'FERMAX','3101201601522585946838..jpg','','','SIM',0,'fermax','0800 724 2200',NULL),(155,'DISPAC','3101201601561402184462..jpg','','','SIM',0,'dispac','( 11 ) 3751-2098',NULL),(156,'WÜRTH','3101201602164883783074..jpg','','','SIM',0,'wurth','( 11 ) 4613-1900',NULL),(157,'3M','3101201602193183243104..jpg','','','SIM',0,'3m','0800 013 2333',NULL),(158,'GLASSEC','3101201602237618201254..jpg','','','SIM',0,'glassec','( 11 ) 4597-8100',NULL),(159,'VITRAL','3101201602273788596697..jpg','','','SIM',0,'vitral','( 61 ) 3462-3200',NULL),(160,'VIDRATTO','3101201602291309749119..jpg','','','SIM',0,'vidratto','( 61 ) 3038-8888',NULL),(161,'DAY BRASIL','3101201602333112733893..jpg','','','SIM',0,'day-brasil','( 61 ) 2103-7400',NULL),(162,'ALUCOMAXX','3101201602378336177749..jpg','','','SIM',0,'alucomaxx','( 11 ) 3392-5836',NULL);
/*!40000 ALTER TABLE `tb_fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_galerias_portifolios`
--

DROP TABLE IF EXISTS `tb_galerias_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaportifolio`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_galerias_portifolios`
--

LOCK TABLES `tb_galerias_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_galerias_portifolios` DISABLE KEYS */;
INSERT INTO `tb_galerias_portifolios` VALUES (49,'2701201612552515951767.jpg','SIM',NULL,NULL,1),(50,'2701201612555322323114.jpg','SIM',NULL,NULL,1),(51,'2701201612559385792641.jpg','SIM',NULL,NULL,1),(52,'2701201612551951997780.jpg','SIM',NULL,NULL,1),(53,'2701201612553863505253.jpg','SIM',NULL,NULL,1),(57,'2801201611335134256825.jpg','SIM',NULL,NULL,2),(58,'2801201611334976963814.jpg','SIM',NULL,NULL,2),(60,'2801201611424112164444.jpg','SIM',NULL,NULL,3),(61,'2801201611435018171349.jpg','SIM',NULL,NULL,3),(62,'2801201611435659002703.jpg','SIM',NULL,NULL,3),(63,'2801201611435890847129.jpg','SIM',NULL,NULL,3),(65,'2801201611475084387701.jpg','SIM',NULL,NULL,4),(66,'2801201611482683978291.jpg','SIM',NULL,NULL,4),(67,'2801201611486948757660.jpg','SIM',NULL,NULL,4),(68,'2801201611486187683679.jpg','SIM',NULL,NULL,4),(69,'2801201612225498684648.jpg','SIM',NULL,NULL,5),(78,'2801201612277162703292.jpg','SIM',NULL,NULL,7),(79,'2801201612276289139348.jpg','SIM',NULL,NULL,7),(81,'0903201602245802589543.jpg','SIM',NULL,NULL,7),(82,'0903201602286017779260.jpg','SIM',NULL,NULL,7),(83,'0903201602335851455384.jpg','SIM',NULL,NULL,7),(84,'0903201602546584685266.jpg','SIM',NULL,NULL,7),(85,'0903201602563513160939.jpg','SIM',NULL,NULL,7),(86,'1003201604265102283685.jpg','SIM',NULL,NULL,7),(90,'1003201604461808190320.jpg','SIM',NULL,NULL,7),(91,'2103201602353206629703.jpg','SIM',NULL,NULL,5),(92,'2103201602359567190541.jpg','SIM',NULL,NULL,5),(93,'2103201602389481284759.jpg','SIM',NULL,NULL,5),(95,'2103201602406219642543.jpg','SIM',NULL,NULL,5),(96,'2103201602446211789068.jpg','SIM',NULL,NULL,5),(97,'1602201702359484059243.jpg','SIM',NULL,NULL,2),(98,'1602201702396949548559.jpg','SIM',NULL,NULL,2),(99,'1602201702593143294258.jpg','SIM',NULL,NULL,2),(100,'1602201702596271824166.jpg','SIM',NULL,NULL,2),(101,'1602201703082784145759.jpg','SIM',NULL,NULL,5),(102,'1602201703089075670370.jpg','SIM',NULL,NULL,5),(104,'1602201703081703010114.jpg','SIM',NULL,NULL,5),(105,'1702201702114012074193.jpg','SIM',NULL,NULL,3),(106,'1702201702118576060020.jpg','SIM',NULL,NULL,3),(107,'1702201702118050043168.jpg','SIM',NULL,NULL,3),(108,'1702201702119627042483.jpg','SIM',NULL,NULL,3),(110,'1702201702164103397341.jpg','SIM',NULL,NULL,3),(111,'1702201702233584748568.jpg','SIM',NULL,NULL,3),(112,'2002201709543475957910.jpg','SIM',NULL,NULL,8),(113,'2002201709544482516311.jpg','SIM',NULL,NULL,8),(114,'2002201709543445714190.jpg','SIM',NULL,NULL,8),(115,'2002201709547850116483.jpg','SIM',NULL,NULL,8),(116,'2002201709543370450544.jpg','SIM',NULL,NULL,8),(117,'2302201703346961081930.jpg','SIM',NULL,NULL,7),(118,'2302201703347777199389.jpg','SIM',NULL,NULL,7),(119,'2302201703353788762994.jpg','SIM',NULL,NULL,7),(120,'2302201703359036268322.jpg','SIM',NULL,NULL,7),(121,'2302201703372931011840.jpg','SIM',NULL,NULL,1),(122,'2302201703425814184960.jpg','SIM',NULL,NULL,7),(123,'2403201710513273901947.jpg','SIM',NULL,NULL,9),(124,'2403201710517244544674.jpg','SIM',NULL,NULL,9),(125,'2403201710518746244543.jpg','SIM',NULL,NULL,9),(126,'2403201710511181205506.jpg','SIM',NULL,NULL,9),(127,'2403201710515235487226.jpg','SIM',NULL,NULL,9),(128,'2403201710527931159519.jpg','SIM',NULL,NULL,9);
/*!40000 ALTER TABLE `tb_galerias_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_galerias_produtos`
--

DROP TABLE IF EXISTS `tb_galerias_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_galerias_produtos`
--

LOCK TABLES `tb_galerias_produtos` WRITE;
/*!40000 ALTER TABLE `tb_galerias_produtos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_galerias_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logins`
--

DROP TABLE IF EXISTS `tb_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logins`
--

LOCK TABLES `tb_logins` WRITE;
/*!40000 ALTER TABLE `tb_logins` DISABLE KEYS */;
INSERT INTO `tb_logins` VALUES (1,'HomeWeb','e10adc3949ba59abbe56e057f20f883e','SIM',0,'atendimento.sites@homewebbrasil.com.br'),(2,'Alkha','05f3acd148e26384247c5fd8bff63c6e','SIM',0,'alkha@grupoalkha.com.br'),(3,'Alkha','8ff774ccf3c5b1c242e303a0e9714f1c','SIM',0,'alkha@alkhaaluminio.com.br'),(4,'Alkha','8ff774ccf3c5b1c242e303a0e9714f1c','SIM',0,'alkha@alkhaaluminio.com.br');
/*!40000 ALTER TABLE `tb_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_logs_logins`
--

DROP TABLE IF EXISTS `tb_logs_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB AUTO_INCREMENT=1783 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_logs_logins`
--

LOCK TABLES `tb_logs_logins` WRITE;
/*!40000 ALTER TABLE `tb_logs_logins` DISABLE KEYS */;
INSERT INTO `tb_logs_logins` VALUES (1580,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:01:00',5),(1581,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:02:10',5),(1582,'ALTERAÇÃO DO CLIENTE ','','2015-09-25','18:02:57',5),(1583,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:25',1),(1584,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:33',1),(1585,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:41',1),(1586,'CADASTRO DO CLIENTE ','','2015-09-29','15:57:48',1),(1587,'CADASTRO DO CLIENTE ','','2015-09-29','16:03:55',1),(1588,'CADASTRO DO CLIENTE ','','2015-09-29','16:04:19',1),(1589,'CADASTRO DO CLIENTE ','','2015-09-29','16:04:43',1),(1590,'CADASTRO DO CLIENTE ','','2015-09-29','16:11:11',1),(1591,'CADASTRO DO CLIENTE ','','2015-09-30','20:23:24',1),(1592,'CADASTRO DO CLIENTE ','','2015-09-30','20:23:56',1),(1593,'CADASTRO DO CLIENTE ','','2015-09-30','20:26:24',1),(1594,'CADASTRO DO CLIENTE ','','2015-09-30','20:27:36',1),(1595,'CADASTRO DO CLIENTE ','','2015-09-30','20:29:37',1),(1596,'ALTERAÇÃO DO CLIENTE ','','2015-09-30','20:39:32',1),(1597,'CADASTRO DO CLIENTE ','','2015-09-30','20:50:23',1),(1598,'CADASTRO DO CLIENTE ','','2015-10-13','16:29:17',5),(1599,'CADASTRO DO CLIENTE ','','2015-10-13','16:53:37',5),(1600,'CADASTRO DO CLIENTE ','','2015-10-13','16:53:54',5),(1601,'CADASTRO DO CLIENTE ','','2015-10-13','16:54:00',5),(1602,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:54:12',5),(1603,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:54:17',5),(1604,'CADASTRO DO CLIENTE ','','2015-10-13','16:54:53',5),(1605,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','16:55:07',5),(1606,'CADASTRO DO CLIENTE ','','2015-10-13','17:02:26',5),(1607,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:03:37',5),(1608,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:17:15',5),(1609,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:17:23',5),(1610,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:19:05',5),(1611,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:26:44',5),(1612,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','17:39:10',5),(1613,'CADASTRO DO CLIENTE ','','2015-10-13','18:13:51',1),(1614,'CADASTRO DO CLIENTE ','','2015-10-13','18:20:48',1),(1615,'CADASTRO DO CLIENTE ','','2015-10-13','18:21:03',1),(1616,'CADASTRO DO CLIENTE ','','2015-10-13','18:21:22',1),(1617,'CADASTRO DO CLIENTE ','','2015-10-13','18:23:08',1),(1618,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:40:31',1),(1619,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:43:52',1),(1620,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:44:59',1),(1621,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','20:46:05',1),(1622,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:10',1),(1623,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:25',1),(1624,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:42',1),(1625,'CADASTRO DO CLIENTE ','','2015-10-13','21:55:54',1),(1626,'CADASTRO DO CLIENTE ','','2015-10-13','21:56:07',1),(1627,'CADASTRO DO CLIENTE ','','2015-10-13','21:56:25',1),(1628,'ALTERAÇÃO DO CLIENTE ','','2015-10-13','22:53:14',1),(1629,'CADASTRO DO CLIENTE ','','2015-10-13','23:53:22',1),(1630,'CADASTRO DO CLIENTE ','','2015-10-13','23:54:31',1),(1631,'CADASTRO DO CLIENTE ','','2015-10-13','23:55:28',1),(1632,'ALTERAÇÃO DO CLIENTE ','','2015-10-14','00:42:55',1),(1633,'ALTERAÇÃO DO CLIENTE ','','2015-10-14','01:11:23',1),(1634,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:26:00',1),(1635,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:26:50',1),(1636,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:27:25',1),(1637,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:28:36',1),(1638,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:29:04',1),(1639,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:29:50',1),(1640,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:30:32',1),(1641,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:31:51',1),(1642,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:32:32',1),(1643,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:34:03',1),(1644,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:34:44',1),(1645,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:36:26',1),(1646,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:43:54',1),(1647,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:45:18',1),(1648,'ALTERAÇÃO DO CLIENTE ','','2015-12-19','12:51:15',1),(1649,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:47:35',1),(1650,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:49:39',1),(1651,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:52:23',1),(1652,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:53:43',1),(1653,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:54:26',1),(1654,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:57:51',1),(1655,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','12:58:30',1),(1656,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:03:07',1),(1657,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:03:51',1),(1658,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:04:59',1),(1659,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:10:18',1),(1660,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:10:56',1),(1661,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:11:36',1),(1662,'ALTERAÇÃO DO CLIENTE ','','2016-01-04','13:14:51',1),(1663,'CADASTRO DO CLIENTE ','','2016-01-27','12:45:08',1),(1664,'ALTERAÇÃO DO CLIENTE ','','2016-01-27','12:50:17',1),(1665,'EXCLUSÃO DO LOGIN 3, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'3\'','2016-01-27','13:08:15',1),(1666,'EXCLUSÃO DO LOGIN 4, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'','2016-01-27','13:08:29',1),(1667,'EXCLUSÃO DO LOGIN 5, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'','2016-01-27','13:08:41',1),(1668,'EXCLUSÃO DO LOGIN 6, NOME: , Email: ','DELETE FROM tb_depoimentos WHERE iddepoimento = \'6\'','2016-01-27','13:08:52',1),(1669,'ALTERAÇÃO DO CLIENTE ','','2016-01-27','13:16:18',1),(1670,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:30:41',1),(1671,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:31:11',1),(1672,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:32:14',1),(1673,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:00',1),(1674,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:21',1),(1675,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:35:52',1),(1676,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:36:05',1),(1677,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:42:21',1),(1678,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:47:34',1),(1679,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:52:01',1),(1680,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:53:00',1),(1681,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','11:54:06',1),(1682,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:21:47',1),(1683,'CADASTRO DO CLIENTE ','','2016-01-28','12:26:52',1),(1684,'EXCLUSÃO DO LOGIN 6, NOME: , Email: ','DELETE FROM tb_portifolios WHERE idportifolio = \'6\'','2016-01-28','12:27:46',1),(1685,'DESATIVOU O LOGIN 50','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'50\'','2016-01-28','12:31:40',1),(1686,'DESATIVOU O LOGIN 51','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'51\'','2016-01-28','12:31:54',1),(1687,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:33:07',1),(1688,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:33:59',1),(1689,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:34:10',1),(1690,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:37:50',1),(1691,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:38:43',1),(1692,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:40:02',1),(1693,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:42:16',1),(1694,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:42:51',1),(1695,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:43:18',1),(1696,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:43:51',1),(1697,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:44:18',1),(1698,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:44:40',1),(1699,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:45:11',1),(1700,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:45:53',1),(1701,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:46:30',1),(1702,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:53:54',1),(1703,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','12:54:24',1),(1704,'EXCLUSÃO DO LOGIN 9, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'9\'','2016-01-28','13:06:05',1),(1705,'EXCLUSÃO DO LOGIN 10, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'10\'','2016-01-28','13:06:11',1),(1706,'EXCLUSÃO DO LOGIN 11, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'11\'','2016-01-28','13:06:17',1),(1707,'EXCLUSÃO DO LOGIN 12, NOME: , Email: ','DELETE FROM tb_clientes WHERE idcliente = \'12\'','2016-01-28','13:06:23',1),(1708,'ALTERAÇÃO DO CLIENTE ','','2016-01-28','13:09:33',1),(1709,'DESATIVOU O LOGIN 150','UPDATE tb_fornecedores SET ativo = \'NAO\' WHERE idfornecedor = \'150\'','2016-01-29','18:06:12',1),(1710,'DESATIVOU O LOGIN 151','UPDATE tb_fornecedores SET ativo = \'NAO\' WHERE idfornecedor = \'151\'','2016-01-29','18:06:23',1),(1711,'DESATIVOU O LOGIN 152','UPDATE tb_fornecedores SET ativo = \'NAO\' WHERE idfornecedor = \'152\'','2016-01-29','18:06:36',1),(1712,'EXCLUSÃO DO LOGIN 42, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'42\'','2016-01-29','18:06:51',1),(1713,'EXCLUSÃO DO LOGIN 43, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'43\'','2016-01-29','18:06:56',1),(1714,'EXCLUSÃO DO LOGIN 44, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'44\'','2016-01-29','18:07:03',1),(1715,'EXCLUSÃO DO LOGIN 45, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'45\'','2016-01-29','18:07:08',1),(1716,'EXCLUSÃO DO LOGIN 46, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'46\'','2016-01-29','18:07:13',1),(1717,'EXCLUSÃO DO LOGIN 47, NOME: , Email: ','DELETE FROM tb_dicas WHERE iddica = \'47\'','2016-01-29','18:07:19',1),(1718,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','13:32:17',1),(1719,'ATIVOU O LOGIN 150','UPDATE tb_fornecedores SET ativo = \'SIM\' WHERE idfornecedor = \'150\'','2016-01-31','13:32:43',1),(1720,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','13:36:23',1),(1721,'ATIVOU O LOGIN 151','UPDATE tb_fornecedores SET ativo = \'SIM\' WHERE idfornecedor = \'151\'','2016-01-31','13:36:35',1),(1722,'ATIVOU O LOGIN 152','UPDATE tb_fornecedores SET ativo = \'SIM\' WHERE idfornecedor = \'152\'','2016-01-31','13:38:48',1),(1723,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','13:39:15',1),(1724,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','13:43:09',1),(1725,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','13:45:28',1),(1726,'CADASTRO DO CLIENTE ','','2016-01-31','13:49:36',1),(1727,'CADASTRO DO CLIENTE ','','2016-01-31','13:52:54',1),(1728,'CADASTRO DO CLIENTE ','','2016-01-31','13:56:36',1),(1729,'CADASTRO DO CLIENTE ','','2016-01-31','14:16:01',1),(1730,'CADASTRO DO CLIENTE ','','2016-01-31','14:19:48',1),(1731,'CADASTRO DO CLIENTE ','','2016-01-31','14:23:21',1),(1732,'CADASTRO DO CLIENTE ','','2016-01-31','14:26:03',1),(1733,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','14:27:54',1),(1734,'CADASTRO DO CLIENTE ','','2016-01-31','14:29:45',1),(1735,'CADASTRO DO CLIENTE ','','2016-01-31','14:33:17',1),(1736,'CADASTRO DO CLIENTE ','','2016-01-31','14:37:15',1),(1737,'ALTERAÇÃO DO CLIENTE ','','2016-01-31','14:42:30',1),(1738,'ALTERAÇÃO DO CLIENTE ','','2016-02-01','16:54:31',1),(1739,'ALTERAÇÃO DO CLIENTE ','','2016-02-15','17:59:19',1),(1740,'ALTERAÇÃO DO CLIENTE ','','2016-02-23','14:34:33',1),(1741,'ALTERAÇÃO DO CLIENTE ','','2016-02-23','15:02:57',1),(1742,'ALTERAÇÃO DO CLIENTE ','','2016-02-23','15:13:07',1),(1743,'ALTERAÇÃO DO CLIENTE ','','2016-02-23','15:21:07',1),(1744,'ALTERAÇÃO DO CLIENTE ','','2016-03-09','14:04:50',2),(1745,'ALTERAÇÃO DO CLIENTE ','','2016-03-09','14:09:08',2),(1746,'ALTERAÇÃO DO CLIENTE ','','2016-03-23','15:51:27',1),(1747,'ALTERAÇÃO DO CLIENTE ','','2016-04-05','11:11:47',1),(1748,'ALTERAÇÃO DO CLIENTE ','','2016-04-05','11:12:43',1),(1749,'ALTERAÇÃO DO CLIENTE ','','2016-04-05','11:13:01',1),(1750,'ALTERAÇÃO DO CLIENTE ','','2016-04-05','11:37:00',1),(1751,'ALTERAÇÃO DO CLIENTE ','','2016-04-07','16:03:24',1),(1752,'ALTERAÇÃO DO CLIENTE ','','2017-02-15','10:45:12',3),(1753,'ALTERAÇÃO DO CLIENTE ','','2017-02-15','11:18:11',3),(1754,'ALTERAÇÃO DO CLIENTE ','','2017-02-16','14:00:27',3),(1755,'ALTERAÇÃO DO CLIENTE ','','2017-02-16','14:04:43',3),(1756,'DESATIVOU O LOGIN 52','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'52\'','2017-02-17','14:32:03',1),(1757,'ATIVOU O LOGIN 52','UPDATE tb_categorias_portifolios SET ativo = \'SIM\' WHERE idcategoriaportifolio = \'52\'','2017-02-17','14:33:56',1),(1758,'CADASTRO DO CLIENTE ','','2017-02-17','14:39:47',1),(1759,'DESATIVOU O LOGIN 49','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'49\'','2017-02-17','14:40:15',1),(1760,'DESATIVOU O LOGIN 52','UPDATE tb_categorias_portifolios SET ativo = \'NAO\' WHERE idcategoriaportifolio = \'52\'','2017-02-17','14:40:22',1),(1761,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:10',1),(1762,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:17',1),(1763,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:23',1),(1764,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:29',1),(1765,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:35',1),(1766,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:41',1),(1767,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:41:47',1),(1768,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','14:42:03',3),(1769,'EXCLUSÃO DO LOGIN 49, NOME: , Email: ','DELETE FROM tb_categorias_portifolios WHERE idcategoriaportifolio = \'49\'','2017-02-17','15:09:28',1),(1770,'EXCLUSÃO DO LOGIN 50, NOME: , Email: ','DELETE FROM tb_categorias_portifolios WHERE idcategoriaportifolio = \'50\'','2017-02-17','15:09:31',1),(1771,'EXCLUSÃO DO LOGIN 51, NOME: , Email: ','DELETE FROM tb_categorias_portifolios WHERE idcategoriaportifolio = \'51\'','2017-02-17','15:09:34',1),(1772,'EXCLUSÃO DO LOGIN 52, NOME: , Email: ','DELETE FROM tb_categorias_portifolios WHERE idcategoriaportifolio = \'52\'','2017-02-17','15:09:37',1),(1773,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','15:13:42',1),(1774,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','15:27:06',3),(1775,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','15:28:03',3),(1776,'ALTERAÇÃO DO CLIENTE ','','2017-02-17','15:29:17',3),(1777,'CADASTRO DO CLIENTE ','','2017-02-20','09:52:57',3),(1778,'ALTERAÇÃO DO CLIENTE ','','2017-03-01','16:41:05',3),(1779,'CADASTRO DO CLIENTE ','','2017-03-24','10:50:11',3),(1780,'ALTERAÇÃO DO CLIENTE ','','2017-10-03','18:46:46',1),(1781,'ALTERAÇÃO DO CLIENTE ','','2017-10-03','18:56:57',1),(1782,'ALTERAÇÃO DO CLIENTE ','','2017-10-03','19:00:57',1);
/*!40000 ALTER TABLE `tb_logs_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_portifolios`
--

DROP TABLE IF EXISTS `tb_portifolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_portifolios` (
  `idportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_categoriaportifolio` int(11) DEFAULT NULL,
  `cliente` varchar(255) DEFAULT NULL,
  `tipo_servico` varchar(255) DEFAULT NULL,
  `data_feito` date DEFAULT NULL,
  `feito_por` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idportifolio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_portifolios`
--

LOCK TABLES `tb_portifolios` WRITE;
/*!40000 ALTER TABLE `tb_portifolios` DISABLE KEYS */;
INSERT INTO `tb_portifolios` VALUES (1,'E-BUSINESS','1702201703271836921907..jpg','',NULL,NULL,NULL,'SIM',0,'ebusiness',53,'E-BUSINESS','Pele de Vidro Structural Glazing','0000-00-00','Alkha Esquadrias'),(2,'ED. CÔTE DAZUR','1702201703288367676022..jpg','',NULL,NULL,NULL,'SIM',0,'ed-cote-dazur',53,'ED. CÔTE D´AZUR','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(3,'DUE CAPRI e DUE MURANO','1702201702421519073189..jpg','',NULL,NULL,NULL,'SIM',0,'due-capri-e-due-murano',53,'Ed. DUE CAPRI e DUE MURANO','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(4,'ED. LE-CLUB','2801201611476114530012..jpg','',NULL,NULL,NULL,'SIM',0,'ed-leclub',53,'LE-CLUB','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(5,'VITRIUM ODEBRECHT','1702201703293976706721..jpg','',NULL,NULL,NULL,'SIM',0,'vitrium-odebrecht',53,'VITRIUM - ODEBRECHT','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(7,'OBRAS RESIDENCIAIS','2801201612268257425087..jpg','',NULL,NULL,NULL,'SIM',0,'obras-residenciais',53,'OBRAS RESIDENCIAIS','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(8,'ED. VIA IBIZA','2002201709528256802165..jpg','',NULL,NULL,NULL,'SIM',NULL,'ed-via-ibiza',53,'ED. VIA IBIZA','Esquadrias de Alumínio','0000-00-00','Alkha Esquadrias'),(9,'PRAÇA CAPITAL','2403201710502402182635..jpg','',NULL,NULL,NULL,'SIM',NULL,'praca-capital',53,'Ed. Praça Capital','Esquadrias de Alumínio','0000-00-00','Alucan Esquadrias');
/*!40000 ALTER TABLE `tb_portifolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_produtos`
--

DROP TABLE IF EXISTS `tb_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_produtos`
--

LOCK TABLES `tb_produtos` WRITE;
/*!40000 ALTER TABLE `tb_produtos` DISABLE KEYS */;
INSERT INTO `tb_produtos` VALUES (146,'Brises','0401201601106939749336..jpg','<p>\r\n	Brises s&atilde;o particularmente &uacute;teis para fachadas, quando se necessita diminuir a a&ccedil;&atilde;o da luz direta ou quando se requer privacidade sem perder a luminosidade, ventila&ccedil;&atilde;o e vis&atilde;o exterior.</p>\r\n<p>\r\n	Funciona ainda como elemento decorativo para fachadas, dando um ar de leveza mesmo quando utilizado em grandes pain&eacute;is.</p>','','','','SIM',0,'brises',0,NULL,NULL,NULL),(147,'Revestimento ACM','3009201508231400491918..jpg','<p>\r\n	Material inovador e nobre, o ACM &eacute; um produto com certifica&ccedil;&atilde;o ISO-9002 &nbsp;revolucion&aacute;rio e vers&aacute;til produzido com a mais alta tecnologia. &Eacute; rigorosamente plano e ao mesmo tempo de alta conformabilidade, ou seja, dobr&aacute;-lo mediante usinagem ou curv&aacute;-lo atrav&eacute;s calandra. &nbsp; Usado como revestimento de &nbsp;edifica&ccedil;&otilde;es externos e internos, postos de gasolinas, quiosques, Bancos etc.</p>\r\n<p>\r\n	Os pain&eacute;is ACM s&atilde;o compostos de duas laminas de alum&iacute;nio unidas a um nucleo termoplastico de polietileno de baixa densidade. S&atilde;o fabricados em um processo continuo em placas de 3, 4, 5, 6, 8 mm de espessura, com v&aacute;rias combina&ccedil;&otilde;es de comprimentos e larguras. o paniel acm &eacute; tambem conhecido como alucobond.</p>','','','','SIM',0,'revestimento-acm',0,NULL,NULL,NULL),(148,'Portões de Alumínio','0103201704417015483847..jpg','<p>\r\n	Trabalhamos com os mais diversos projetos de port&otilde;es: alum&iacute;nio anodizado e pintura. Port&otilde;es basculantes, deslizantes e pivotantes.</p>','','','','SIM',0,'portoes-de-aluminio',0,NULL,NULL,NULL),(149,'Pele de Vidro','1602201702049729887466..jpg','<p>\r\n	A t&eacute;cnica de &quot;Pele de Vidro&quot; &eacute; muito usada pelos arquitetos e engenheiros para proporcionar grandes aberturas, criando fachadas atraentes e consistentes. Esta t&eacute;cnica se aplica com o objetivo de permitir uma instala&ccedil;&atilde;o mais &aacute;gil, econ&ocirc;mica e de melhor veda&ccedil;&atilde;o quando comparada &agrave; metodologia tradicional. &nbsp;A instala&ccedil;&atilde;o padr&atilde;o se faz com quadros pr&eacute;-montados que chegam &agrave; obra com os vidros e borrachas previamente fixados e prontos para o encaixe nas travessas que se fixam na face frontal das colunas.<span id=\"\\&quot;\\\\&quot;cke_bm_113E\\\\&quot;\\&quot;\" span=\"\\&quot;\\&quot;\">&nbsp;</span></p>','','','','SIM',0,'pele-de-vidro',0,NULL,NULL,NULL),(150,'Portas de Alumínio','0903201602041648438361..jpg','<div>\r\n	Trabalhamos com os mais variados tipos de portas para atender suas necessidades ex: portas de abrir e correr. Tipo, pivotante, integrada, anti - ru&iacute;do, e etc.</div>\r\n<div>\r\n	Solicite uma visita de nosso profissional</div>','','','','SIM',0,'portas-de-aluminio',0,NULL,NULL,NULL),(151,'Janelas de Alumínio','0903201602097777469981..jpg','<div>\r\n	Trabalhamos com os mais variados tipos de janelas para atender suas necessidades ex: janelas de abrir e correr. Tipo basculante, maxim-ar, pivotante, fixa, integrada, anti - ru&iacute;do, grades de prote&ccedil;&atilde;o para janelas e etc.</div>\r\n<div>\r\n	Solicite uma visita de nosso profissional</div>','','','','SIM',0,'janelas-de-aluminio',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'alkhaaluminio'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-05 18:41:46

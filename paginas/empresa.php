
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>





</head>
<body class="bg-empresa">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!-- barra-internas-->
    <div class="container sombra-barra-internas ">
        <div class="row">
          <div class="container">
            <div class="row">
              <div class="col-xs-4 barra-interna text-center">
                <ol class="breadcrumb ">
                  <li><span >você esta em:</span></li>
                  <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
                  <li class="active">Empresa</li>
              </ol>
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
          </div>
      </div>
  </div>
</div>
</div>
<!-- barra-internas-->


<!-- descricao empresa fundo degrader -->
<div class="container-fluir fundo-cinza-degrader">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class=" col-xs-12 descricao-empresa">

                 <div class="col-xs-8 col-xs-offset-4 top50 bottom50">
                    <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                    <p><?php Util::imprime($dados[descricao]) ?></p>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<!-- descricao empresa fundo degrader -->







<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>

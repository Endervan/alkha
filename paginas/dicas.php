
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>

</head>
<body class="bg-dicas">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!-- barra-internas-->
    <div class="container sombra-barra-internas ">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 barra-interna text-center">
                        <ol class="breadcrumb ">
                            <li><span >você esta em:</span></li>
                            <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
                            <li class="active">Dicas</li>
                        </ol>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- barra-internas-->






    <!-- dicas gerais -->
    <div class="container">
        <div class="row">


            <?php
            $result = $obj_site->select("tb_dicas");
            if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result))
                {
                    ?>
                    <div class="col-xs-3 descricao-dicas-gerais">

                        <div class="col-xs-12">
                            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                            </a>
                        </div>

                        <div class="col-xs-12 descricao-dicas-gerais1 ">
                            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                                <h2><i class="fa fa-comment-o"></i>12</h2>
                                <p><?php Util::imprime($row[descricao], 180) ?></p>
                                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-dicas.png" alt="">
                            </a>
                        </div>
                    </div>
                    <?php
                }
            } ?>



        </div>
    </div>
    <!-- dicas gerais -->


    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

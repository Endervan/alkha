<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!--    ==============================================================  -->
    <!--    ROYAL SLIDER    -->
    <!--    ==============================================================  -->

    <!-- slider JS files -->
    <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
    <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
    <script>
    jQuery(document).ready(function($) {
        // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
        // it's recommended to disable them when using autoHeight module
        $('#content-slider-1').royalSlider({
            autoHeight: true,
            arrowsNav: true,
            fadeinLoadedSlide: false,
            controlNavigationSpacing: 0,
            controlNavigation: 'bullets',
            imageScaleMode: 'none',
            imageAlignCenter: false,
            loop: true,
            loopRewind: true,
            numImagesToPreload: 6,
            keyboardNavEnabled: true,
            usePreloader: false,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true
            }

        });
    });
    </script>


</head>
<body>
    <!-- ==============================================================  -->
    <!--  BANNERS   -->
    <div class="container-fluid">
        <div class="row">
            <div id="container_banner">
                <div id="content_slider">
                    <div id="content-slider-1" class="contentSlider rsDefault">

                        <?php
                        $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand()");
                        if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){

                                if($row[url] != ''){
                                    ?>
                                    <div>
                                        <a href="<?php Util::imprime($row[url]) ?>">
                                            <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="">
                                        </a>
                                    </div>
                                    <?php
                                }else{
                                    ?>
                                    <div>
                                        <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="">
                                    </div>
                                    <?php
                                }

                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  BANNERS   -->
    <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!-- conteudo -->
    <div class="pagina">

        <!-- topo -->
        <?php require_once('./includes/topo.php') ?>
        <!-- topo -->
    </div>
    <!-- conteudo -->
    <!--  ==============================================================  -->

    <!-- produtos home -->
    <div class="container">
        <div class="row">
            <div class="produtos-home">
                <!-- categorias -->
                <?php
                $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
                if(mysql_num_rows($result) > 0){
                    while($row = mysql_fetch_array($result))
                    {
                        ?>
                        <div class="col-xs-6 pbottom30">

                            <div class="col-xs-6">
                                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>">
                                </a>
                            </div>

                            <div class="col-xs-6 fundo-laranja">

                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-branca-produtos.png" alt="">
                                <p><?php Util::imprime($row[descricao], 300) ?></p>
                                <div class="text-center top10">
                                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary btn-transparente text-center">
                                        SAIBA MAIS
                                    </a>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>


            </div>
        </div>
    </div>
    <!-- produtos home -->

    <!-- portifolio-home -->
    <div class="container-fluir bg-cinza pbottom300">
        <div class="row">
            <div class="container top40">
                <div class="row">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-portifolio-home.png" alt="">
                    <div class="col-xs-12 descricao-portifolio-home">
                        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
                        <h3><?php Util::imprime($dados[descricao]) ?></h3>
                        <!-- botoes saiba mais home -->
                        <a href="<?php echo Util::caminho_projeto() ?>/portfolios/" class="btn btn-primary btn-transparente1">
                            TODOS
                        </a>

                        <?php
                        $result = $obj_site->select("tb_categorias_portifolios");
                        if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){
                                ?>
                                <a href="<?php echo Util::caminho_projeto() ?>/portfolios/<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-primary btn-transparente1">
                                    <?php Util::imprime($row[titulo]) ?>
                                </a>
                                <?php
                            }
                        }
                        ?>

                        <!-- botoes saiba mais home -->
                    </div>


                    <div class="">
                        <!-- portifolio-fotos -->

                        <?php
                        $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 6");
                        if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){
                                ?>

                                <div class="col-xs-4 view view-second" style="margin:0px;padding:0px;">
                                    <!-- portifolio-fotos com effect css -->

                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>">

                                    <div class="mask"></div>
                                    <div class="content input100">
                                        <h2><?php Util::imprime($row[titulo]) ?></h2>
                                        <p><?php Util::imprime(Util::troca_value_nome($row[id_categoriaportifolio], "tb_categorias_portifolios", "idcategoriaportifolio", "titulo")) ?></p>
                                        <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                        <!-- portifolio-fotos -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- portifolio-home -->

    <!-- conheca mais a alka -->
    <div class="container posicao bottom100">
        <div class="row">
            <div class="conheca-alka-home text-right">
                <h2>CONHEÇA MAIS A ALKHA</h2>
            </div>

            <div class="col-xs-10 col-xs-offset-1 fundo-laranja-altura">

                <div class="col-xs-6 ">
                    <a href="<?php echo Util::caminho_projeto() ?>/empresa">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/conheca-alkha-home.jpg" alt="">
                    </a>
                </div>
                <div class="col-xs-6 top20">
                    <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
                    <p><?php Util::imprime($dados[descricao]) ?></p>

                    <div class="text-right top30">
                        <a href="<?php echo Util::caminho_projeto() ?>/empresa" class="btn btn-primary btn-transparente text-center">
                            SAIBA MAIS
                        </a>
                    </div>

                </div>

            </div>
            <div class="posicao-sombra-home">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/sombra-saiba-mais-home.png" alt="">
            </div>
        </div>
    </div>
    <!-- conheca mais a alka -->



    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

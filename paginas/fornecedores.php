
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>





</head>
<body class="bg-fornecedores">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!-- barra-internas-->
    <div class="container sombra-barra-internas ">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 barra-interna text-center">
                        <ol class="breadcrumb ">
                            <li><span >você esta em:</span></li>
                            <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
                            <li class="active">Fornecedores</li>
                        </ol>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- barra-internas-->

    <!-- descricao fornecedores -->
    <div class="container">
        <div class="row">
            <div class="descricao-fornecedores top30 bottom40">
                <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
                <p><?php Util::imprime($dados[descricao]) ?></p>
            </div>
        </div>
    </div>
    <!-- descricao fornecedores -->


    <!-- fornecedores -->
    <div class="container-fluir fundo-laranja1 pbottom20">
        <div class="row">

            <div class="container ">
                <div class="row">

                    <div class="nosso-clientes-empresa text-center">
                        <h3>LISTA DE FORNECEDORES</h3>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-branca-produtos.png" alt="">
                    </div>

                    <div class="nosso-clientes-empresa11 pbottom20">

                        <?php
                        $result = $obj_site->select("tb_fornecedores");
                        if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result))
                            {
                                ?>
                                <div class="col-xs-3 pbottom20">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>">
                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                    <h2><?php Util::imprime($row[telefone]) ?></h2>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <!-- fornecedores -->


    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

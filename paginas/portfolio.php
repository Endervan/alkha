<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_portifolios", $complemento);

if(mysql_num_rows($result)==0)
{
    Util::script_location(Util::caminho_projeto()."/portfolio");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- carroucel com js -->

    <!-- carroucel com js -->



</head>
<body class="bg-portfolio">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!-- barra-internas-->
    <div class="container sombra-barra-internas ">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 barra-interna text-center">
                        <ol class="breadcrumb ">
                            <li><span >você esta em:</span></li>
                            <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
                            <li class="active">Portfólio</li>
                        </ol>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- barra-internas-->


    <!-- descricao portifolio usando carroucel -->
    <div class="container bottom100 top30">
        <div class="row">
            <div class="col-xs-8">
                <?php
                $result = $obj_site->select("tb_galerias_portifolios", "AND id_portifolio = '$dados_dentro[0]'");

                if (mysql_num_rows($result) > 0) {
                    ?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            while($row = mysql_fetch_array($result)){

                                //  selecionador da classe active
                                if($i == 0){
                                    $active = 'active';
                                }else{
                                    $active = '';
                                }
                                ?>
                                <li data-target="#carousel-id" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>
                                <?php
                                $imagens[] = $row;
                                $i++;
                            }
                            ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                            $i = 0;
                            if (count($imagens) > 0) {
                                foreach($imagens as $imagem){

                                    //  selecionador da classe active
                                    if($i == 0){
                                        $active = 'active';
                                    }else{
                                        $active = '';
                                    }

                                    ?>
                                    <div class="item <?php echo $active ?>">
                                        <?php $obj_site->redimensiona_imagem("../uploads/$imagem[imagem]", 770, 578, array('class'=>'input100')); ?>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>

                        </div>


                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>


                    </div>
                    <?php
                }
                ?>
            </div>
            <!-- sobre projeto -->
            <div class="col-xs-4">
                <div class="portifolio-descricao">
                    <div class="text-right">
                        <h2>SOBRE O PROJETO</h2>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-portifolio-dentro.png" alt="">
                    </div>

                    <h6 class="top15"><?php Util::imprime($dados_dentro[titulo]) ?></h6>

                    <p><?php Util::imprime($dados_dentro[descricao]) ?></p>

                    <div class="col-xs-12 top10 bottom50">
                        <div class="row top15">
                            <div class="col-xs-6 top5">
                                <h3><i class="fa fa-user"></i>FEITO POR:</h3>
                            </div>
                            <div class="col-xs 6">
                                <span><?php Util::imprime($dados_dentro[feito_por]) ?></span>
                            </div>
                        </div>




                        <div class="row top15">
                            <div class="col-xs-6 top5">
                                <h3><i class="fa fa-lightbulb-o"></i>TIPO DE SERVIÇO:</h3>
                            </div>
                            <div class="col-xs 6">
                                <span><?php Util::imprime($dados_dentro[tipo_servico]) ?></span>
                            </div>
                        </div>


                        <div class="row top15">
                            <div class="col-xs-6 top5">
                                <h3><i class="fa fa-link"></i>CLIENTES:</h3>
                            </div>
                            <div class="col-xs 6">
                                <span><?php Util::imprime($dados_dentro[cliente]) ?></span>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <!-- sobre projeto -->

        </div>
    </div>

    <!-- mais projeto -->
    <div class="container-fluir bg-cinza pbottom60 bottom100">
        <div class="row">
            <div class="container top40">
                <div class="row">
                    <div class="posicao-ajustada1">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-personalizada-produtos-interno1.png" alt="">
                    </div>

                    <div class="top60">
                        <!-- portifolio-fotos -->
                        <?php
                        $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 3");
                        if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){
                                ?>
                                <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>">
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 387, 390); ?>
                                </a>
                                <?php
                            }
                        }
                        ?>
                        <!-- portifolio-fotos -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mais projeto -->

    <!-- descricao portifolio usando carroucel -->





    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

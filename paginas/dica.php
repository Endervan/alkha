<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
    Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>


</head>
<body class="bg-dicas">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <!-- barra-internas-->
    <div class="container sombra-barra-internas ">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 barra-interna text-center">
                        <ol class="breadcrumb ">
                            <li><span >você esta em:</span></li>
                            <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
                            <li class="active">Dicas</li>
                        </ol>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- barra-internas-->





    <!-- dicas dentro -->
    <div class="container">
        <div class="row">

            <div class="col-xs-6">
                <div class="descricao-dicas-interna">
                    <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
                    <h2><i class="fa fa-calendar"></i><span>20/10/2015</span><i class="fa fa-comment-o"></i>12</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-8 descricao-dicas-interna">

                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>" alt="" class="input100">

                    <p><?php Util::imprime($dados_dentro[descricao]) ?></p>

                    <!-- deixe seu comentario -->
                    <div class="">


                        <div class="col-xs-12">

                            <?php
                            if (isset($_POST[comentario])) {
                                $obj_site->insert("tb_comentarios_dicas");
                                Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
                            }
                            ?>

                            <form class="form_comentario" method="post" action="">
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">DEIXE SEU COMENTÁRIO</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="recipient-nome" class="control-label">NOME:</label>
                                                    <input type="text" name="nome" class="form-control" id="recipient-nome">
                                                </div>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">E-MAIL:</label>
                                                    <input type="text" name="email" class="form-control" id="recipient-name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">MENSAGEM:</label>
                                                    <textarea class="form-control" name="comentario" id="message-text"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
                                                <button type="submit" class="btn btn-primary">ENVIAR</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="id_dica" value="<?php echo $dados_dentro[0] ?>">
                                <input type="hidden" name="ativo" value="NAO">
                                <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

                            </form>
                        </div>
                        <!-- boostrap modal -->
                    </div>





                    <?php $result = $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]' ORDER BY data DESC"); ?>

                    <div class="descricao-comentario">
                        <div class="col-xs-6 top30">
                            <h3><span> <?php echo mysql_num_rows($result) ?> </span> COMENTARIO(S)</h3>
                        </div>


                        <!-- boostrap modal -->
                        <div class=" col-xs-6 text-right bottom40">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">DEIXE SEU COMENTÁRIO</button>
                        </div>
                    </div>

                    <?php
                    if(mysql_num_rows($result) > 0)
                    {
                        while($row = mysql_fetch_array($result))
                        {
                            ?>
                            <div class="row">
                                <div class="col-xs-12 bottom40">
                                    <div class="col-xs-1 descricao-comentario-dentro1">
                                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/foto-descricao-comentario.png" class="img-circle" alt="">
                                    </div>
                                    <div class="col-xs-11">
                                        <div class="descricao-comentario-dentro">
                                            <h3><?php Util::imprime($row[nome]) ?></h3>
                                            <p><?php Util::imprime($row[comentario]) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        echo "<h1>Nenhum comentário. Seja o primeiro a comentar.</h1><br />";
                    }
                    ?>


                    <!-- deixe seu comentario -->


                </div>

                <div class="col-xs-4">
                    <!-- Nav tabs -->
                    <ul class=" tabs-dicas nav nav-tabs  text-center" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">OS MAIS ACESSADOS</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">OS ÚLITMOS POSTS</a></li>
                    </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">

                    <!-- mais acessados dentro da dica internas-->
                    <div role="tabpanel" class="tab-pane fade in active" id="home">


                        <?php
                        $result = $obj_site->select("tb_dicas", "ORDER BY rand() LIMIT 4");
                        if(mysql_num_rows($result) > 0)
                        {
                            while($row = mysql_fetch_array($result))
                            {
                                ?>
                                <div class="col-xs-4 descricao-dicas-interna1">

                                    <div class="col-xs-5">
                                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 125, 125, array('class'=>'img-circle')); ?>
                                        </a>
                                    </div>

                                    <div class="col-xs-7 ">
                                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                                            <h2><i class="fa fa-comment-o"></i> <?php echo mysql_num_rows( $obj_site->select("tb_comentarios_dicas", "AND id_dica = $row[0]") ) ?></h2>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        } ?>



                    </div>

                    <!-- mais acessados dentro da dica internas-->



                    <!-- utimos posts -->
                    <div role="tabpanel" class="tab-pane fade" id="profile">

                        <?php
                        $result = $obj_site->select("tb_dicas", "ORDER BY iddica LIMIT 4");
                        if(mysql_num_rows($result) > 0)
                        {
                            while($row = mysql_fetch_array($result))
                            {
                                ?>
                                <div class="col-xs-4 descricao-dicas-interna1">

                                    <div class="col-xs-5">
                                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 125, 125, array('class'=>'img-circle')); ?>
                                        </a>
                                    </div>

                                    <div class="col-xs-7 ">
                                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                                            <h2><i class="fa fa-comment-o"></i> <?php echo mysql_num_rows( $obj_site->select("tb_comentarios_dicas", "AND id_dica = $row[0]") ) ?></h2>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        } ?>



                    </div>
                    <!-- utimos posts -->

                </div>
                <!-- Tab panes -->


            </div>
        </div>
    </div>




    <!-- dicas dentro -->


    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>




<script>
$(document).ready(function() {
    $('.form_comentario').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            assunto: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            comentario: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>

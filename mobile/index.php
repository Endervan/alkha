<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
    <?php require_once('./includes/head.php'); ?>


</head>

<body>


    <?php require_once('./includes/topo.php'); ?>



    <!-- slider -->
    <div class="container">
        <div class="row slider-index">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php
                    $result = $obj_site->select("tb_banners", "AND tipo_banner = 2 ORDER BY rand() LIMIT 6");
                    if (mysql_num_rows($result) > 0) {
                        $i = 0;
                        while($row = mysql_fetch_array($result)){

                            //  selecionador da classe active
                            if($i == 0){
                                $active = 'active';
                            }else{
                                $active = '';
                            }

                            ?>

                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>

                            <?php
                            $imagens[] = $row;
                            $i++;
                        }
                    }
                    ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $i = 0;
                    if (count($imagens) > 0) {
                        foreach($imagens as $imagem){

                            //  selecionador da classe active
                            if($i == 0){
                                $active = 'active';
                            }else{
                                $active = '';
                            }

                            ?>
                            <div class="item <?php echo $active ?>">
                                <?php
                                if ($imagem[url] != '') {
                                    ?>
                                    <a href="<?php Util::imprime($imagem[url]) ?>">
                                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
                                    </a>
                                    <?php
                                }else{
                                    ?>
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
                                    <?php
                                }
                                ?>

                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- slider -->


    <!-- produtos -->
    <div class="container">
        <div class="row top25">
            <div class="col-xs-12 bottom25">
                <div class="titulo-duas-linhas text-center">
                    <h1>PRODUTOS</h1>
                </div>
            </div>


            <?php
            $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
            if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                    ?>
                    <div class="col-xs-6">
                        <div class="lista-produto">
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                            </a>
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                            <div class="text-center linha-prod">
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="SAIBA MAIS" class="btn btn-transparente">SAIBA MAIS</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>




        </div>
    </div>
    <!-- produtos -->



    <!-- portifolio -->
    <div class="container">
        <div class="row bg-portifolio top25">
            <div class="col-xs-12">
                <ul class="lista-categorias">
                    <li class="btn-33"><a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio" class="btn btn-preto input100">TODOS</a></li>
                    <?php
                    $result = $obj_site->select("tb_categorias_portifolios", "ORDER BY rand() LIMIT 5");
                    if(mysql_num_rows($result) > 0){
                        while($row = mysql_fetch_array($result)){
                            ?>
                            <li class="btn-33"><a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/?categoria=<?php echo $row[0] ?>" class="btn btn-preto input100"><?php Util::imprime($row[titulo]) ?></a></li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-xs-12 top25">
                <?php
                $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 4");
                if(mysql_num_rows($result) > 0){
                    while($row = mysql_fetch_array($result)){
                        ?>
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]) ?>" class="pull-left">
                            <?php $obj_site->redimensiona_imagem("../uploads/tumb_$row[imagem]", 225, 225); ?>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- portifolio -->


    <!-- conheca mais a alkha -->
    <div class="container">
        <div class="row index-conheca-alkha">
            <div class="col-xs-12 ">
                <h1>CONHEÇA MAIS A ALKHA</h1>
            </div>
            <div class="col-xs-12 top15">
                <div class="index-conheca-alkha-conteudo">
                    <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
                    <p><?php Util::imprime($dados[descricao]) ?></p>
                    <div class="text-center linha-prod top35 bottom25">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" title="SAIBA MAIS" class="btn btn-transparente">SAIBA MAIS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- conheca mais a alkha -->


    





    <?php require_once('./includes/rodape.php'); ?>


</body>

</html>

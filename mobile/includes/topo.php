<div class="container top5 topo">

    <!-- telefone -->
    <div class="row">
        <div class="col-xs-12 telefone-topo text-right">
            <span class="right30 tel"><?php Util::imprime($config[telefone1]) ?></span>
            <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-preto" title="CHAMAR">
                CHAMAR
            </a>
        </div>
    </div>
    <!-- telefone -->

    <!-- menu -->
    <div class="row bg-topo top5">
        <div class="col-xs-6 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
            </a>
        </div>
        <div class="col-xs-6">
            <select class="menu-home input100 top20" id="menu-site">
                <option value="">NOSSO MENU</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa/">A EMPRESA</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/">PRODUTOS</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/">PORTFÓLIO</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/fornecedores/">FORNECEDORES</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/">ORÇAMENTO</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos/">CONTATOS</option>
            </select>
        </div>
    </div>
    <!-- menu -->

</div>

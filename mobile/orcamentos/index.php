<?php
ob_start();
session_start();
require_once("../../class/Include.class.php");
$obj_site = new Site();


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
	//  SELECIONO O TIPO
	switch($_GET[tipo])
	{
		case "produto":
		$id = $_GET[id];
		unset($_SESSION[solicitacoes_produtos][$id]);
		sort($_SESSION[solicitacoes_produtos]);
		break;
		case "servico":
		$id = $_GET[id];
		unset($_SESSION[solicitacoes_servicos][$id]);
		sort($_SESSION[solicitacoes_servicos]);
		break;
	}

}

?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>
</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>


	<form class="form-inline FormContato" role="form" method="post">



		<!-- barra-internas-->
		<div class="container sombra-barra-internas">
			<div class="row">
				<div class="col-xs-6 barra-interna text-right">
					<ol class="breadcrumb ">
						<li class="active">Fornecedores</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- barra-internas-->



		<?php
        //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
        if(isset($_POST[nome]))
        {

            //  CADASTRO OS PRODUTOS SOLICITADOS
            for($i=0; $i < count($_POST[qtd]); $i++)
            {
                $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

                $mensagem .= "
                            <tr>
                            <td><p>". $_POST[qtd][$i] ."</p></td>
                            <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                            </tr>
                            ";
            }




            //  ENVIANDO A MENSAGEM PARA O CLIENTE
            $texto_mensagem = "
            O seguinte cliente fez uma solicitação pelo site. <br />


            Assunto: ". ($_POST[assunto]) ." <br />
            Nome: ". ($_POST[nome]) ." <br />
            Email: ". ($_POST[email]) ." <br />
            Telefone: ". ($_POST[telefone]) ." <br />
            Como conheceu a Alkha: ". ($_POST[como_conheceu]) ." <br />
            Mensagem: <br />
            ". nl2br($_POST[mensagem]) ." <br />


            <br />
            <h2> Serviços selecionados:</h2> <br />

            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

            <tr>
            <td><h4>QTD</h4></td>
            <td><h4>SERVIÇO</h4></td>
            </tr>
            $mensagem

            </table>
            ";


            Util::envia_email($config[email], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            unset($_SESSION[solicitacoes_produtos]);
            unset($_SESSION[solicitacoes_servicos]);
            Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

        }
        ?>


		<!-- fazendo orcamento selecionavel marcio fazer-->
		<div class="container fundo-laranja1 pbottom20 ">
			<div class="row">
				<div class="nosso-clientes-empresa text-center">
					<h3>SERVIÇOS DESEJADOS</h3>
					<img src="../imgs/barra-branca-produtos.png" alt="">
				</div>

				<div class="col-xs-12">
					<div class="selecao-orcamentos">



						<div class="lista-itens-orcamento">

							<?php
							if(count($_SESSION[solicitacoes_produtos]) > 0)
							{
								?>
								<table class="table top20 tb-lista-itens">
									<thead>
										<tr>
											<th>ITEM</th>
											<th>DESCRIÇÃO</th>
											<th class="text-center">QTD</th>
											<th class="text-center">EXCLUIR</th>
										</tr>
									</thead>
									<tbody>
										<?php
										for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
										{
											$row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
											?>
											<tr>
												<td><img src="../../uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="74" width="48" alt=""></td>
												<td><?php Util::imprime($row[titulo]) ?></td>
												<td class="text-center">
													<input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
													<input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
												</td>
												<td class="text-center">
													<a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
														<i class="glyphicon glyphicon-remove link-excluir"></i>
													</a>
												</td>
											</tr>
											<?php
										}
										?>
									</tbody>
								</table>
								<?php
							}
							?>
						</div>

						<div class="row top15">
							<div class="col-xs-12">
								<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-default">
									Continuar orçando
								</a>
							</div>
						</div>



					</div>
				</div>


			</div>
		</div>
		<!-- fazendo orcamento selecionavel -->



		<!-- formulario -orcamentos -->
		<div class="container top20 bottom30">
			<div class="row">
				<div class="col-xs-12">

					<div class="descricao-produtos-internos1 text-right">
						<h1> CONFIRME SEUS DADOS </h1>
						<img class="bottom40" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra-descricao-orcamento1.png" alt="">
					</div>



					<div class="row">
						<div class="col-xs-6 form-group ">
							<label class="glyphicon glyphicon-user"> <span>Nome</span></label>
							<input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
						</div>
						<div class="col-xs-6 form-group">
							<label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
							<input type="text" name="email" class="form-control fundo-form input100" placeholder="">
						</div>
					</div>



					<div class="row">
						<div class="col-xs-6 top20 form-group">
							<label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
							<input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
						</div>
						<div class="col-xs-6 top20 form-group">
							<label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
							<input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
						</div>

					</div>

					<div class="row">
						<div class="col-xs-6 top20 form-group">
							<label class="glyphicon glyphicon-globe"> <span>Cidade</span></label>
							<input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
						</div>
						<div class="col-xs-6 top20 form-group">
							<label class="glyphicon glyphicon-globe"> <span>Estado</span></label>
							<input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 top20 form-group">
                            <label class="glyphicon glyphicon-star"> <span>Como você conheceu o site da ALKHA?</span></label>
                            <select class="form-control input100" name="como_conheceu">
                                <option value="">Selecione</option>
                                <option value="Google">Google</option>
                                <option value="Internet">Internet</option>
                                <option value="Jornais">Jornais</option>
                                <option value="Rádio">Rádio</option>
                                <option value="TV">Tv</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>
					</div>

					<div class="row">
						<div class="col-xs-12 top20 form-group">
							<label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
							<textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
						</div>

					</div>


					<div class="text-right top30 col-xs-12">
						<button type="submit" class="btn btn-orcamento" name="btn_contato">
							ENVIAR
						</button>
					</div>


				</div>

			</div>




		</div>


	</form>
	<!-- formulario -orcamentos -->

	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>




<script>
$(document).ready(function() {
	$('.FormContato').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {

					}
				}
			},
			email: {
				validators: {
					notEmpty: {

					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {

					}
				}
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			como_conheceu: {
				validators: {
					notEmpty: {

					}
				}
			},
			mensagem: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>

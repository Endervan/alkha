<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
	$complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_portifolios",$complemento);
if(mysql_num_rows($result)==0)
{
	Util::script_location(Util::caminho_projeto()."/mobile/portifolio/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Portfólio</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->

	<div class="container">
		<div class="row">
			<div class="col-xs-12 decricao-produtos-dentro text-right">
				<h3 class="">SOBRE O PROJETO</h3>
				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra-produtos-dentro.png" alt="">
				<div class=" decricao-produtos-dentro1">
					<h1 class="bottom15 top15"><?php Util::imprime($dados_dentro[titulo]) ?></h1>
					<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>" alt="" class="input100">
					<p class="top15"><?php Util::imprime($dados_dentro[descricao]) ?></p>
				</div>
			</div>
			<!-- descricao-produtos  -->
		</div>


		<div class="row portifolio-descricao">
			<div class="col-xs-12 top20 bottom30">
				<div class="row top15">
					<div class="col-xs-6 top5">
						<h3><i class="fa fa-user"></i>FEITO POR:</h3>
					</div>
					<div class="col-xs 6">
						<span><?php Util::imprime($dados_dentro[feito_por]) ?></span>
					</div>
				</div>



				<div class="row top15">
					<div class="col-xs-6 top5">
						<h3><i class="fa fa-lightbulb-o"></i>TIPO DE SERVIÇO:</h3>
					</div>
					<div class="col-xs 6">
						<span><?php Util::imprime($dados_dentro[tipo_servico]) ?></span>
					</div>
				</div>


				<div class="row top15">
					<div class="col-xs-6 top5">
						<h3><i class="fa fa-link"></i>CLIENTES:</h3>
					</div>
					<div class="col-xs 6">
						<span><?php Util::imprime($dados_dentro[cliente]) ?></span>
					</div>
				</div>


			</div>

		</div>
	</div>

	<!-- carroucel-porifolio dentro -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<?php
				$result = $obj_site->select("tb_galerias_portifolios", "AND id_portifolio = '$dados_dentro[0]'");

				if (mysql_num_rows($result) > 0) {
					?>

					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators navs-personalizados3">
							<?php
							$i = 0;
							while($row = mysql_fetch_array($result)){

								//  selecionador da classe active
								if($i == 0){
									$active = 'active';
								}else{
									$active = '';
								}
								?>
								<li data-target="#carousel-id" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>
								<?php
								$imagens[] = $row;
								$i++;
							}
							?>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php
							$i = 0;
							if (count($imagens) > 0) {
								foreach($imagens as $imagem){

									//  selecionador da classe active
									if($i == 0){
										$active = 'active';
									}else{
										$active = '';
									}

									?>
									<div class="item <?php echo $active ?>">
										<?php $obj_site->redimensiona_imagem("../uploads/$imagem[imagem]", 448, 337); ?>
									</div>
									<?php
									$i++;
								}
							}
							?>

						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>

					</div>
					<?php
				}
				?>

			</div>
		</div>
	</div>
	<!-- carroucel-porifolio dentro -->


	<!-- projetos realizados -->
	<div class="container bg-cinza1 pbottom40 top40">
		<div class="row">

			<div class=" text-center top30">

				<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/mais-projeto-portifolio-barra.png" alt="">
			</div>




			<div class="col-xs-12 pbottom20 posicao-reajuste top20">
				<!-- portifolio-fotos -->
				<?php
				$result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 2");
				if(mysql_num_rows($result) > 0){
					while($row = mysql_fetch_array($result)){
						?>
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]) ?>">
							<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 223, 225); ?>
						</a>
						<?php
					}
				}
				?>
				<!-- portifolio-fotos -->

			</div>

		</div>

	</div>
	<!-- projetos realizados -->

	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>

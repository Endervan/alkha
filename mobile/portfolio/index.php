<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Portfólio</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->




	<!-- effect css imagens do portifolio geral -->

	<div class="container top20">
		<div class="row">
			<div class="col-xs-12 descricao-portifolio-home">
				<!-- botoes saiba mais home -->
				<div class="col-xs-4">
					<a href="<?php echo Util::caminho_projeto(); ?>/mobile/portfolio" class="btn btn-primary btn-transparente-portifolio input98">
						TODOS
					</a>
				</div>




				<?php
				$result = $obj_site->select("tb_categorias_portifolios");
				if(mysql_num_rows($result) > 0){
					while($row = mysql_fetch_array($result)){
						?>
						<div class="col-xs-4">
							<a href="<?php echo Util::caminho_projeto(); ?>/mobile/portfolio/?categoria=<?php Util::imprime($row[0]); ?>" class="btn btn-primary btn-transparente-portifolio input98">
								<?php Util::imprime($row[titulo]); ?>
							</a>
						</div>
						<?php
					}
				}
				?>




				<!-- botoes saiba mais home -->
			</div>
		</div>

		<div class="row bottom10">


			<?php
			if ($_GET[categoria] != '') {
				$complemento = "AND id_categoriaportifolio = '$_GET[categoria]' ";
			}

			$result = $obj_site->select("tb_portifolios", $complemento);
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					?>
					<div class="lista-portifolio col-xs-6">
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]) ?>">
							<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 215, 225); ?>
							<h1><?php Util::imprime($row[titulo]) ?></h1>
							<p><?php Util::imprime(Util::troca_value_nome($row[id_categoriaportifolio], "tb_categorias_portifolios", "idcategoriaportifolio", "titulo")) ?></p>
						</a>
					</div>
					<?php
				}
			}
			?>

		</div>

	</div>

	<!-- effect css imagens do portifolio geral -->




	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>

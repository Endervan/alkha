<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Produtos</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->




	<!-- produtos -->
	<div class="container top40">
		<!-- row a cda 12 col -->
		<div class="row">

			<?php
            $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
            if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                    ?>
                    <div class="col-xs-6">
                        <div class="lista-produto">
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                            </a>
                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                            <div class="text-center linha-prod">
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="SAIBA MAIS" class="btn btn-transparente">SAIBA MAIS</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

		</div>






	</div>
	<!-- produtos -->





	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>

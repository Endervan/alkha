<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

	<!-- /*effect pinterest nas fotos*/ -->

	<script  src="<?php echo Util::caminho_projeto() ?>/dist/js/jquery.jsonp-2.1.4.min.js"></script>
	<script src="<?php echo Util::caminho_projeto() ?>/dist/js/jaliswall.js"></script>

	<script>
	@media (max-width: 640px) {

		.wall-column { width: 50%; }

	}

	@media (max-width: 480px) {

		.wall-column {
			width: auto;
			float: none;
		}

	}

	</script>

	<script>

	$('.wall').jaliswall({

		// item classname
		item : '.wall-item',

		// column classname
		columnClass : '.wall-column'

	});

	</script>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Dicas</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->

	<!-- dicas dentro -->

	<!-- /*effect pinterest nas fotos*/ -->
	<div class="container top30">
		<div class="row">

			<?php
			$result = $obj_site->select("tb_dicas");
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					?>
					<div class="col-xs-6 descricao-dicas-interna1 bottom40">
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]) ?>">
							<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
							<h1><?php Util::imprime($row[titulo]) ?></h1>
							<h2><i class="fa fa-comment-o"></i> <?php echo mysql_num_rows( $obj_site->select("tb_comentarios_dicas", "AND id_dica = $row[0]") ) ?></h2>
							<p><?php Util::imprime($row[descricao], 150) ?></p>
						</a>
					</div>
					<?php
				}
			}
			?>
		</div>


</div>




<!-- dicas dentro -->


<!-- rodape -->
<?php require_once('../includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>

<script>
$(document).ready(function() {
	$('.FormContato').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {

					}
				}
			},
			email: {
				validators: {
					notEmpty: {

					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {

					}
				}
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			mensagem: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>



<script>
$(document).ready(function() {
	$('.FormCurriculo').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {

					}
				}
			},
			email: {
				validators: {
					notEmpty: {

					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {

					}
				}
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			escolaridade: {
				validators: {
					notEmpty: {

					}
				}
			},
			cargo: {
				validators: {
					notEmpty: {

					}
				}
			},
			area: {
				validators: {
					notEmpty: {

					}
				}
			},
			curriculo: {
				validators: {
					notEmpty: {
						message: 'Por favor insira seu currículo'
					},
					file: {
						extension: 'doc,docx,pdf,rtf',
						type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
						maxSize: 5*1024*1024,   // 5 MB
						message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
					}
				}
			},
			mensagem: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>

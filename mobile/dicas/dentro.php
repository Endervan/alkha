<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
	$complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_dicas",$complemento);
if(mysql_num_rows($result)==0)
{
	Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Dicas</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->

	<!-- dicas dentro -->
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="descricao-dicas-interna">
					<h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
					<h2><i class="fa fa-comment-o"></i><?php echo mysql_num_rows( $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]'") ) ?></h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 descricao-dicas-interna">

				<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>" alt="" class="input100">


				<p><?php Util::imprime($dados_dentro[descricao]) ?></p>

				<!-- deixe seu comentario -->
				<div class="">
					<div class="descricao-comentario">
						<div class="col-xs-6 top20">
							<h3><span><?php echo mysql_num_rows( $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]'") ) ?></span> COMENTARIOS</h3>
						</div>


						<!-- boostrap modal -->
						<div class=" col-xs-6 text-right bottom40">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">DEIXE SEU COMENTÁRIO</button>
						</div>
					</div>

					<div class="col-xs-12">

						<?php
						if (isset($_POST[comentario])) {
							$obj_site->insert("tb_comentarios_dicas");
							Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
						}
						?>



						<form class="form_comentario" method="post" action="">
							<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="exampleModalLabel">DEIXE SEU COMENTÁRIO</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label for="recipient-nome" class="control-label">NOME:</label>
												<input type="text" name="nome" class="form-control" id="recipient-nome">
											</div>
											<div class="form-group">
												<label for="recipient-name" class="control-label">E-MAIL:</label>
												<input type="text" name="email" class="form-control" id="recipient-name">
											</div>
											<div class="form-group">
												<label for="message-text" class="control-label">MENSAGEM:</label>
												<textarea class="form-control" name="comentario" id="message-text"></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
											<button type="submit" class="btn btn-primary">ENVIAR</button>
										</div>
									</div>
								</div>
							</div>

							<input type="hidden" name="id_dica" value="<?php echo $dados_dentro[0] ?>">
							<input type="hidden" name="ativo" value="NAO">
							<input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

						</form>
					</div>
					<!-- boostrap modal -->
				</div>


				<!-- depoimentos -->
				<?php
				$result = $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]' ORDER BY data DESC");
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
					?>
					<div class="row bottom40">
						<div class="col-xs-2 descricao-comentario-dentro1">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/foto-descricao-comentario.png" class="img-circle input100" alt="">
						</div>
						<div class="col-xs-10">
							<div class="descricao-comentario-dentro">
								<h3><?php Util::imprime($row[nome]) ?></h3>
								<p><?php Util::imprime($row[comentario]) ?></p>
							</div>
						</div>
					</div>
					<?php
					}
				}
				?>
				<!-- depoimentos -->


				<!-- deixe seu comentario -->


			</div>

		</div>

	</div>




	<!-- dicas dentro -->


	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>

<script>
$(document).ready(function() {
	$('.FormContato').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {

					}
				}
			},
			email: {
				validators: {
					notEmpty: {

					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {

					}
				}
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			comentario: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>



<script>
$(document).ready(function() {
	$('.FormCurriculo').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			nome: {
				validators: {
					notEmpty: {

					}
				}
			},
			email: {
				validators: {
					notEmpty: {

					},
					emailAddress: {
						message: 'Esse endereço de email não é válido'
					}
				}
			},
			telefone: {
				validators: {
					notEmpty: {

					}
				}
			},
			assunto: {
				validators: {
					notEmpty: {

					}
				}
			},
			escolaridade: {
				validators: {
					notEmpty: {

					}
				}
			},
			cargo: {
				validators: {
					notEmpty: {

					}
				}
			},
			area: {
				validators: {
					notEmpty: {

					}
				}
			},
			curriculo: {
				validators: {
					notEmpty: {
						message: 'Por favor insira seu currículo'
					},
					file: {
						extension: 'doc,docx,pdf,rtf',
						type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
						maxSize: 5*1024*1024,   // 5 MB
						message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
					}
				}
			},
			mensagem: {
				validators: {
					notEmpty: {

					}
				}
			}
		}
	});
});
</script>

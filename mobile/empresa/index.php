<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



	<!-- barra-internas-->
	<div class="container sombra-barra-internas">
		<div class="row">
			<div class="col-xs-6 barra-interna text-right">
				<ol class="breadcrumb ">
					<li class="active">Empresa</li>
				</ol>
			</div>
		</div>
	</div>
	<!-- barra-internas-->

	<!-- descricao empresa -->
	<div class="container">
		<div class="row">
			<div class="fundo-transparente-empresa">

			</div>

			<div class="col-xs-12 descricao-empresa">
				<?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
				<h2><?php Util::imprime($dados[descricao]) ?></h2>
			</div>
		</div>
	</div>
	<!-- descricao empresa -->





	<?php require_once('../includes/rodape.php'); ?>

</body>
</html>

<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>


</head>

<body class="bg-empresa">


  <?php require_once('../includes/topo.php'); ?>



  <!-- barra-internas-->
  <div class="container sombra-barra-internas">
    <div class="row">
      <div class="col-xs-6 barra-interna text-right">
        <ol class="breadcrumb ">
          <li class="active">Fornecedores</li>
        </ol>
      </div>  
    </div>
  </div>
  <!-- barra-internas-->

  <!-- descricao fornecedores -->
  <div class="container">
    <div class="row">
      <div class=" col-xs-12 descricao-fornecedores top20 bottom30">
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <p><?php Util::imprime($dados[descricao]) ?></p>
      </div>
    </div>
  </div>
  <!-- descricao fornecedores -->


  <!-- fornecedores -->

  <div class="container ">
    <div class="row fundo-laranja1 pbottom20">
      

      <?php
      $result = $obj_site->select("tb_fornecedores");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result))
        {
        ?>
          <div class="col-xs-6 nosso-clientes-empresa11 top30">
            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <h2><?php Util::imprime($row[telefone]); ?></h2>
          </div>
        <?php 
        }
      }
      ?>


  </div>
  </div>
  <!-- fornecedores -->


  <!-- rodape -->
  <?php require_once('../includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>

<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>



<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>

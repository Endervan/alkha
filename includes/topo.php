<!-- contatos-topo -->
<div class="container">
    <div class="row">

        <div class="col-xs-8 text-right topo-telefone">
            <h1 class="top10">FALE CONOSCO: <span><?php Util::imprime($config[telefone1]) ?></span> </h1>
        </div>

        <div class="col-xs-4">
            <div class="dropdown">
                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn-meu-orcamento">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-cotatos-topo.png" alt="">
                </a>
                <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

                    <h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h6>

                    <?php
                    if(count($_SESSION[solicitacoes_produtos]) > 0)
                    {
                        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                        {
                            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                            ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="lista-itens-carrinho">
                                        <div class="col-xs-2">
                                            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                                        </div>
                                        <div class="col-xs-8">
                                            <h1><?php Util::imprime($row[titulo]) ?></h1>
                                        </div>
                                        <div class="col-xs-1">
                                            <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>

                    <div class="text-right bottom20">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-laranja">
                            FINALIZAR
                        </a>
                    </div>


                </div>

            </div>
        </div>


    </div>
</div>
<!-- contatos-topo -->

<!-- menu-topo -->
<div class="container-fluir fundo-preto top20">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 top10">
                    <a href="<?php echo Util::caminho_projeto() ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
                    </a>
                </div>
                <div class="col-xs-9">
                    <!-- menu -->
                    <nav class="navbar navbar-default navbar-left fundo-navs" role="navigation">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav navs-li">
                                <li class="active">
                                    <a href="<?php echo Util::caminho_projeto() ?>">

                                        HOME
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Util::caminho_projeto() ?>/empresa">

                                        A EMPRESA
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Util::caminho_projeto() ?>/produtos">

                                        PRODUTOS
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Util::caminho_projeto() ?>/portfolios">

                                        PORTFÓLIO
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Util::caminho_projeto() ?>/fornecedores">

                                        FORNECEDORES
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo Util::caminho_projeto() ?>/contato">

                                        CONTATOS
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                    <!-- menu -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- menu-topo -->

<div class="container-fluid rodape-cinza">
	<div class="row">

		<!-- menu -->
		 <div class="container bg-menu-rodape top40">
		 	<div class="row">
		 		<div class="col-xs-12">
			 		<ul class="menu-rodape">
			 			<li>
			 				<a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
			 				<a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
			 				<a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
			 				<a href="<?php echo Util::caminho_projeto() ?>/portfolios">PORTFÓLIO</a>
			 				<a href="<?php echo Util::caminho_projeto() ?>/fornecedores">FORNECEDORES</a>
			 				<a href="<?php echo Util::caminho_projeto() ?>/contato">CONTATOS</a>
			 			</li>
			 		</ul>
		 		</div>
		 	</div>
		 </div>
		 <!-- menu -->

		 <!-- logo, endereco, telefone -->
		<div class="container top40">
			<div class="row">
				<div class="col-xs-3">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
					</a>
				</div>
				<div class="col-xs-7">
					<p class="bottom10"><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]) ?></p>
					<p><i class="glyphicon glyphicon-phone"></i><?php Util::imprime($config[telefone1]) ?></p>
				</div>
				<div class="col-xs-2">

					<?php if ($config[google_plus] != "") { ?> 
						 <a class="pull-left top25 left50" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" > 
							 <p class=""><i style="color: #fff;" class="fa fa-google-plus"></i></p> 
						 </a> 
					 <?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>
			</div>
		</div>
		 <!-- logo, endereco, telefone -->

	</div>
</div>

<div class="container-fluid rodape-preto">
	<div class="row">
			<h5 class="text-center">© Copyright Alkha Esquadrias de Alumínio</h5>
	</div>
</div>
